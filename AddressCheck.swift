//
//  AddressCheck.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 19/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import TinyLog
import MapKit
import CoreLocation




class AddressCheck: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            
               var placemarks = placemarks
               var location = placemarks?.first?.location
            
            print(location?.coordinate.latitude)
            print(location?.coordinate.longitude)
            
        }
        
        
        //getAddressFromLatLon(pdblLatitude:"23.0277432"
        //,withLongitude:"72.5109347")
        // Do any additional setup after loading the view.
    }
    
    let address = "1 Infinite Loop, Cupertino, CA 95014"
    
    let geoCoder = CLGeocoder()
 
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                }
        })
        
    }
}
