import Foundation
import UIKit



extension MyItemVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ItemType == 0{
            return 1
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyItemVcTableViewReusebleCell", for: indexPath) as! MyItemTableViewCell
        
        if ItemType == 0 {
            
            cell.btn_Switch.isHidden = false
            cell.btn_Switch.isHidden = false
            cell.lbl_Date.text = "1-april-2018"
            cell.lbl_ModuleName.text = "Hello"
            cell.img_MyItemImage.image = #imageLiteral(resourceName: "back_cover.jpg")
            
            return cell
        }
        
        cell.btn_Switch.isHidden = true
        cell.btn_Switch.isHidden = true
        cell.lbl_Date.text = "20-april-2018"
        cell.lbl_ModuleName.text = "Good"
        cell.img_MyItemImage.image = #imageLiteral(resourceName: "back_cover.jpg")
        
        //cell?.img_MyItemImage.image = #imageLiteral(resourceName: "back_cover.jpg")
        return cell
    }
}
















