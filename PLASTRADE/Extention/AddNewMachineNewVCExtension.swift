//
//  AddNewMachineNewVCExtension.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 03/07/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import Foundation
import UIKit

//Mark:- picker view datasource & delegate
extension AddNewMachineNewVC : UIPickerViewDataSource, UIPickerViewDelegate,UITextFieldDelegate  {
    
    
    // data method to return the number of column shown in the picker.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // data method to return the number of row shown in the picker.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        print(CheckTextField)
        if  CheckTextField == 1 {
            return MainDataType.count
        }
        else if CheckTextField == 2 {
            return SeleceCategeryForInjection.count
        }
        else if CheckTextField == 3 {
            return PlasticInjectionMachine.count
        }
        else if CheckTextField == 4 {
            return SeleceCategeryForBlow.count
        }
        else if CheckTextField == 5 {
            return PetStretchBlowMouldingMachinetype.count
        }
        else if CheckTextField == 6 {
            return PetBlowMouldingMachinetype.count
        }
        return 0
    }
    
    // delegate method to return the value shown in the picker
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String{
        
        if CheckTextField == 1 {
            return MainDataType[row]
        }
        else if CheckTextField == 2 {
            return SeleceCategeryForInjection[row]
        }
        else if CheckTextField == 3 {
            return PlasticInjectionMachine[row]
        }
        else if CheckTextField == 4 {
            return SeleceCategeryForBlow[row]
        }
        else if CheckTextField == 5 {
            return PetStretchBlowMouldingMachinetype[row]
        }
        else if CheckTextField == 6 {
            return PetBlowMouldingMachinetype[row]
        }
        
        return "Nil"
    }
    
    // delegate method called when the row was selected.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if CheckTextField == 1 {
            txt_SelectMouldingType.text = MainDataType[row]
        }
        else if CheckTextField == 2 {
            txt_SelectMouldingCategory.text = SeleceCategeryForInjection[row]
        }
        else if CheckTextField == 3 {
            txt_MachineType.text = PlasticInjectionMachine[row]
        }
        else if CheckTextField == 4 {
            txt_SelectMouldingCategory.text = SeleceCategeryForBlow[row]
        }
        else if CheckTextField == 5 {
            txt_MachineType.text = PetStretchBlowMouldingMachinetype[row]
        }
        else if CheckTextField == 6 {
            txt_MachineType.text = PetBlowMouldingMachinetype[row]
        }
    }
    
    //Mark:- texfield Delegat method
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Mark:- open PickerView on Touch textfield
        if textField == txt_SelectMouldingType {
            
            txt_SelectMouldingType.inputView     = pickerView
            CheckTextField = 1
            txt_SelectMouldingType.text = "Select Moulding Type"
            txt_MachineType.text = "Select Machine Type"
            txt_SelectMouldingCategory.text = "Select Moulding Category"
            print("You edit myTextField")
        }
            
        else if textField == txt_SelectMouldingCategory {
            
            txt_SelectMouldingCategory.text = "Select Moulding Category"
            
            if txt_SelectMouldingType.text == "Select Moulding Type" {
                showAlert(title: "Select Moulding Category", message: "OK")
            }
                
            else {
                txt_SelectMouldingCategory.inputView = pickerView
                
                if txt_SelectMouldingType.text == "Injection Moulding" {
                    CheckTextField = 2
                }
                
                if txt_SelectMouldingType.text == "Blow Moulding" {
                    CheckTextField = 4
                }
            }
        }
            
        else if textField == txt_MachineType {
            
            txt_MachineType.text = "Select Machine Type"
            
            if txt_SelectMouldingType.text == "Select Moulding Type" {
                showAlert(title: "Select Molding Type", message: "OK")
            }
                
            else if txt_SelectMouldingCategory.text == "Select Moulding Category"{
                showAlert(title: "Select Moulding Category", message: "OK")
            }
                
            else {
                txt_MachineType.inputView = pickerView
                if txt_SelectMouldingCategory.text == "Plastic Injection Moulding"{
                    CheckTextField = 3
                    print("3")
                }
                if txt_SelectMouldingCategory.text == "Pet Stretch Blow Moulding" {
                    CheckTextField = 5
                }
                if txt_SelectMouldingCategory.text ==  "Plastic Blow Moulding" {
                    CheckTextField = 6
                }
            }
        }
    }
}
//---------------------------------------------------------------------------------




