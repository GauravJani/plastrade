//
//  AddNewItemVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 27/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import Foundation
import UIKit



//Mark:- PickerView Delegate & Datasource Methods Extantion
extension AddNewItemVC : UIPickerViewDelegate, UIPickerViewDataSource{
    
    // data method to return the number of column shown in the picker.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // data method to return the number of row shown in the picker.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if ChecktextField == 2 {
            print(MachineSubetypeName1.count)
            return MachineSubetypeName1.count
        }
        if ChecktextField == 1 {
            
            print(MachineTypesNAme.count)
            return MachineTypesNAme.count
        }
        return 0
    }
    
    // delegate method to return the value shown in the picker
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String{
        
        if ChecktextField == 1 {
            
            return  MachineTypesNAme[row]
        }
        
        if ChecktextField == 2 {
            
            print(MachineSubetypeName1[row])
            return MachineSubetypeName1[row]
        }
        
        return "Black Data"
    }
    
    // delegate method called when the row was selected.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        //print("value: \(MachineTypesNAme[row])")
        
        if ChecktextField == 1 {
            txt_pasticModulMachinename.text = MachineTypesNAme[row]
        }
        if ChecktextField == 2 {
            txt_TypeOfMachine.text = MachineSubetypeName1[row]
        }
    }
}
//---------------------------------------------------------------------------------

// Mark :- UITextField Delegate Mathod Extention
extension AddNewItemVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txt_pasticModulMachinename{
            
            ChecktextField = 1
        }
        
        if textField == txt_TypeOfMachine{
            
            ChecktextField = 2
            
        }
        return true
    }
}
//----------------------------------------------------------
