//
//  HomeVcExtension.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 06/07/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation



extension HomeVCViewController : CLLocationManagerDelegate {
    
    
    //MARK:- Get User Location--------------------------------------------------------
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?[0]
                self.displayLocationInfo(pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(_ placemark: CLPlacemark?) {
        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            
            
               lbl_Location.text = locality
            
               print(locality)
               print(administrativeArea)
               print(country)
            
            //            localityTxtField.text = locality
            //            postalCodeTxtField.text = postalCode
            //            aAreaTxtField.text = administrativeArea
            //            countryTxtField.text = country
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    //--------------------------------------------------------
    
    
    
    
}
