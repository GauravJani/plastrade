//
//  File.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 26/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import Foundation
import UIKit


extension SearchMachineVC: UITableViewDelegate, UITableViewDataSource {
    
    
    
    //Mark :- TableView Data Source Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.resultSearchController.isActive {
            
            if filteredTableData.count == 0 {
                return self.tabledata.count
            }
            else{
                return self.filteredTableData.count
            }
        }
        else{
            return self.tabledata.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVCell", for: indexPath) as? SearchVCTableViewCell
        
        //        cell?.Image_Product_image.image = a
        //        cell?.Outlet_MachineName.text = "Hello"
        //        cell?.Outlet_ModuleName.text = "Sc"
        //
        if self.resultSearchController.isActive {
            
            if self.resultSearchController.isActive {
                
                if filteredTableData.count == 0 {
                    cell?.Outlet_MachineName.text = tabledata[indexPath.row]
                    cell?.Image_Product_image.image = a
                }
                else{
                    cell?.Outlet_MachineName.text = filteredTableData[indexPath.row]
                    cell?.Image_Product_image.image = #imageLiteral(resourceName: "PaytmImg")
                }
            }
            else {
                cell?.Outlet_MachineName.text = tabledata[indexPath.row]
                cell?.Image_Product_image.image = a
            }
        }
            
        else {
            cell?.Outlet_MachineName.text = tabledata[indexPath.row]
            cell?.Image_Product_image.image = a
        }
        return cell!
    }
    //-----------------------------------------------------------------//
    
}
