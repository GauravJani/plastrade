//
//	InnerSubType.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class InnerSubType : NSObject, NSCoding{

	var machine : [Machine]!
	var stage : [Machine]!
	var unit : [Machine]!
    var type : [Machine]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: [AnyHashable : Any]){
		if json.isEmpty{
			return
		}
        
        
        type = [Machine]()
        if let TypeArray = json["type"] as? NSArray {
            
            for machineJson in TypeArray{
                let value = Machine(fromJson: machineJson as! [AnyHashable : Any])
                type.append(value)
            }
        }
        
		machine = [Machine]()
		let machineArray = json["machine"] as! NSArray
        
		for machineJson in machineArray{
            let value = Machine(fromJson: machineJson as! [AnyHashable : Any])
            machine.append(value)
		}
        
		    stage = [Machine]()
	 if	let stageArray = json["stage"] as? NSArray
     {
        for stageJson in stageArray {
            let value = Machine(fromJson: stageJson as! [AnyHashable : Any])
			stage.append(value)
		}
        
     }
		unit = [Machine]()
        
	  if let unitArray = json["unit"] as? NSArray
      {
        for unitJson in unitArray{
            let value = Machine(fromJson: unitJson as! [AnyHashable : Any])
			unit.append(value)
		}
        
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if machine != nil{
			var dictionaryElements = [[String:Any]]()
			for machineElement in machine {
				dictionaryElements.append(machineElement.toDictionary())
			}
			dictionary["machine"] = dictionaryElements
		}
		if stage != nil{
			var dictionaryElements = [[String:Any]]()
			for stageElement in stage {
				dictionaryElements.append(stageElement.toDictionary())
			}
			dictionary["stage"] = dictionaryElements
		}
		if unit != nil{
			var dictionaryElements = [[String:Any]]()
			for unitElement in unit {
				dictionaryElements.append(unitElement.toDictionary())
			}
			dictionary["unit"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         machine = aDecoder.decodeObject(forKey: "machine") as? [Machine]
         stage = aDecoder.decodeObject(forKey: "stage") as? [Machine]
         unit = aDecoder.decodeObject(forKey: "unit") as? [Machine]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if machine != nil{
			aCoder.encode(machine, forKey: "machine")
		}
		if stage != nil{
			aCoder.encode(stage, forKey: "stage")
		}
		if unit != nil{
			aCoder.encode(unit, forKey: "unit")
		}

	}

}
