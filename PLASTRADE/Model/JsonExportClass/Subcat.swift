//
//	Subcat.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Subcat : NSObject, NSCoding{

	var innerSubType : InnerSubType!
	var subId : Int!
	var subType : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: [AnyHashable : Any]){
		if json.isEmpty{
			return
		}
		let innerSubTypeJson = json["inner_sub_type"] as! [AnyHashable : Any]
        
		if !innerSubTypeJson.isEmpty{
            innerSubType = InnerSubType(fromJson: innerSubTypeJson as! [AnyHashable : Any])
		}
		subId = json["sub_id"] as! Int
		subType = json["sub_type"] as! String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if innerSubType != nil{
			dictionary["inner_sub_type"] = innerSubType.toDictionary()
		}
		if subId != nil{
			dictionary["sub_id"] = subId
		}
		if subType != nil{
			dictionary["sub_type"] = subType
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         innerSubType = aDecoder.decodeObject(forKey: "inner_sub_type") as? InnerSubType
         subId = aDecoder.decodeObject(forKey: "sub_id") as? Int
         subType = aDecoder.decodeObject(forKey: "sub_type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if innerSubType != nil{
			aCoder.encode(innerSubType, forKey: "inner_sub_type")
		}
		if subId != nil{
			aCoder.encode(subId, forKey: "sub_id")
		}
		if subType != nil{
			aCoder.encode(subType, forKey: "sub_type")
		}

	}

}
