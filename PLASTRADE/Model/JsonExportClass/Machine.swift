//
//	Machine.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Machine : NSObject, NSCoding{

	var id : Int!
	var typeName : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: [AnyHashable : Any]){
		if json.isEmpty{
			return
		}
		id = json["id"] as! Int
        typeName = json["type_name"]  as! String!
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if typeName != nil{
			dictionary["type_name"] = typeName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         typeName = aDecoder.decodeObject(forKey: "type_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if typeName != nil{
			aCoder.encode(typeName, forKey: "type_name")
		}

	}

}
