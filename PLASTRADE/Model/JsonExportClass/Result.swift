//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Result : NSObject, NSCoding{

	var mainId : Int!
	var mainType : String!
	var subcat : [Subcat]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: [AnyHashable : Any]){
		if json.isEmpty{
			return
		}
		mainId = json["main_id"] as! Int
		mainType = json["main_type"] as! String
		subcat = [Subcat]()
		let subcatArray = json["subcat"] as! NSArray
		for subcatJson in subcatArray{
            let value = Subcat(fromJson: subcatJson as! [AnyHashable : Any])
			subcat.append(value)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if mainId != nil{
			dictionary["main_id"] = mainId
		}
		if mainType != nil{
			dictionary["main_type"] = mainType
		}
		if subcat != nil{
			var dictionaryElements = [[String:Any]]()
			for subcatElement in subcat {
				dictionaryElements.append(subcatElement.toDictionary())
			}
			dictionary["subcat"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         mainId = aDecoder.decodeObject(forKey: "main_id") as? Int
         mainType = aDecoder.decodeObject(forKey: "main_type") as? String
         subcat = aDecoder.decodeObject(forKey: "subcat") as? [Subcat]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if mainId != nil{
			aCoder.encode(mainId, forKey: "main_id")
		}
		if mainType != nil{
			aCoder.encode(mainType, forKey: "main_type")
		}
		if subcat != nil{
			aCoder.encode(subcat, forKey: "subcat")
		}

	}

}
