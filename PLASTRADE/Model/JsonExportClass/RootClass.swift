//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class RootClass : NSObject, NSCoding{

	var result : [Result]!
	var success : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: [AnyHashable : Any]!){
		if json.isEmpty{
			return
		}
        
        result = [Result]()
		let resultArray = json["result"] as! NSArray
		for resultJson in resultArray{
            let value = Result(fromJson: resultJson as! [AnyHashable : Any])
			result.append(value)
		}
		success = json["success"] as! Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
        var dictionary = [String:Any]()
		if result != nil{
			var dictionaryElements = [[String:Any]]()
			for resultElement in result {
				dictionaryElements.append(resultElement.toDictionary())
			}
			dictionary["result"] = dictionaryElements
		}
		if success != nil{
			dictionary["success"] = success
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         result = aDecoder.decodeObject(forKey: "result") as? [Result]
         success = aDecoder.decodeObject(forKey: "success") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if result != nil{
			aCoder.encode(result, forKey: "result")
		}
		if success != nil{
			aCoder.encode(success, forKey: "success")
		}

	}

}
