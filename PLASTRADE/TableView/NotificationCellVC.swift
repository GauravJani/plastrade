//
//  NotificationCellVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 23/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class NotificationCellVC: UITableViewCell {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var img_NotificationIMG: UIImageView!
    
    
    @IBOutlet weak var lbl_MachineName: UILabel!
    
    
    
    @IBOutlet weak var lbl_Description: UILabel!
    
}
