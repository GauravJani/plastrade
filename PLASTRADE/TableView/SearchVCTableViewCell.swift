//
//  SearchVCTableViewCell.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 16/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class SearchVCTableViewCell: UITableViewCell {

    @IBOutlet weak var Image_Product_image: UIImageView!
    
    @IBOutlet weak var Outlet_MachineName: UILabel!
    
    @IBOutlet weak var Outlet_ModuleName: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        
        
        Image_Product_image.layer.cornerRadius = Image_Product_image.bounds.height / 2
        
        Image_Product_image.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
