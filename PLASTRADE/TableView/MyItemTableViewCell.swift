//
//  MyItemTableViewCell.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 18/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.

import UIKit

class MyItemTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        
        img_MyItemImage.layer.cornerRadius = img_MyItemImage.bounds.height / 2
        
        
        
        img_MyItemImage.clipsToBounds = true
    }
    
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var btn_Delete: UIButton!
    @IBOutlet weak var btn_Switch: UISwitch!
    @IBOutlet weak var lbl_InjectionName: UILabel!
    
    @IBOutlet weak var img_MyItemImage: UIImageView!
    
    @IBOutlet weak var lbl_ModuleName: UILabel!
    
}
