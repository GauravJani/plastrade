//
//  ServerCall.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 22/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import Foundation
import Alamofire


enum ServerCallName : Int {
    
    case serverCallNameLogin,
    serverCallNameRegister,
    serverCallNameForgotPassword,
    serverCallNameChangePassword,
    serverCallNameVerificationMobileNO,
    serverCallNameGetInjectionModulMachineName,
    serverCallNameUploadImage,
    serverCallNameEditProfile,
    serverCallNameViewProFileData,
    serverCallNameGetTouchWithUS
}

enum HTTP_METHOD {
    case GET,
    POST
}

//MARK: - PROTOCOL
protocol ServerCallDelegate {
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName)
    func ServerCallFailed(_ errorObject:String, name: ServerCallName)
}

//
//MARK: - ServerCall CLASS
class ServerCall: NSObject {
    var delegateObject : ServerCallDelegate!
    
    // Shared Object Creation
    static let sharedInstance = ServerCall()
    
    //MARK: - Request Methods.
    // Make API Request WITHOUT Header Parameters
    func requestWithURL(_ httpMethod: HTTP_METHOD, urlString: String, delegate: ServerCallDelegate, name: ServerCallName) {
        
        self.delegateObject = delegate
        
        let methodOfRequest : HTTPMethod = (httpMethod == HTTP_METHOD.GET) ? HTTPMethod.get : HTTPMethod.post
        let queue = DispatchQueue(label: "com.versatiletechno.manager-response-queue", attributes: DispatchQueue.Attributes.concurrent)
        
        let request = Alamofire.request(urlString, method: methodOfRequest, parameters: nil)
        
        request.responseJSON(queue: queue,
                             options: JSONSerialization.ReadingOptions.allowFragments) {
                                (response : DataResponse<Any>) in
                                // You are now running on the concurrent `queue` you created earlier.
                                print("Parsing JSON on thread: \(Thread.current) is main thread: \(Thread.isMainThread)")
                                
                                // To update anything on the main thread, just jump back on like so.
                                DispatchQueue.main.async {
                                    print("Am I back on the main thread: \(Thread.isMainThread)")
                                    if (response.result.isSuccess) {
                                        self.delegateObject.ServerCallSuccess(response.result.value! as AnyObject, name: name)
                                    }
                                    else if (response.result.isFailure) {
                                        self.delegateObject.ServerCallFailed((response.result.error?.localizedDescription)!, name: name)
                                    }
                                }
        }
    }
    
    // Make API Request WITH Parameters
    func requestWithUrlAndParameters(_ httpMethod: HTTP_METHOD, urlString: String, parameters: [String : AnyObject], delegate: ServerCallDelegate, name: ServerCallName) {
        
        self.delegateObject = delegate
        let methodOfRequest : HTTPMethod = (httpMethod == HTTP_METHOD.GET) ? HTTPMethod.get : HTTPMethod.post
        let queue = DispatchQueue(label: "com.versatiletechno.manager-response-queue", attributes: DispatchQueue.Attributes.concurrent)
        let request = Alamofire.request(urlString, method: methodOfRequest, parameters: parameters)
        request.responseJSON(queue: queue,
                             options: JSONSerialization.ReadingOptions.allowFragments) {
                                (response : DataResponse<Any>) in
                                // You are now running on the concurrent `queue` you created earlier.
                                print("Parsing JSON on thread: \(Thread.current) is main thread: \(Thread.isMainThread)")
                                print("\n\n\n")
                                print("\n")
                                print(parameters)
                                print("\n\n\n")
                                print("\n\n\n")
                                print(response)
                                
                                // To update anything on the main thread, just jump back on like so.
                                DispatchQueue.main.async {
                                    print("Am I back on the main thread: \(Thread.isMainThread)")
                                    if (response.result.isSuccess) {
                                        self.delegateObject.ServerCallSuccess(response.result.value! as AnyObject, name: name)
                                    }
                                    else if (response.result.isFailure) {
                                        self.delegateObject.ServerCallFailed((response.result.error?.localizedDescription)!, name: name)
                                    }
                                }
        }
    }

    // Make API Request WITH Header
    func requestWithUrlAndHeader(_ httpMethod: HTTP_METHOD, urlString: String, header: [String : String], delegate: ServerCallDelegate, name: ServerCallName) {
        self.delegateObject = delegate
        let methodOfRequest : HTTPMethod = (httpMethod == HTTP_METHOD.GET) ? HTTPMethod.get : HTTPMethod.post
        let queue = DispatchQueue(label: "com.versatiletechno.manager-response-queue", attributes: DispatchQueue.Attributes.concurrent)
        let request = Alamofire.request(urlString, method: methodOfRequest, parameters: nil, headers: header)
        request.responseJSON(queue: queue,
                             options: JSONSerialization.ReadingOptions.allowFragments) {
                                (response : DataResponse<Any>) in
                                // You are now running on the concurrent `queue` you created earlier.
                                print("Parsing JSON on thread: \(Thread.current) is main thread: \(Thread.isMainThread)")
                                
                                // To update anything on the main thread, just jump back on like so.
                                DispatchQueue.main.async {
                                    print("Am I back on the main thread: \(Thread.isMainThread)")
                                    if (response.result.isSuccess) {
                                        self.delegateObject.ServerCallSuccess(response.result.value! as AnyObject, name: name)
                                    }
                                    else if (response.result.isFailure) {
                                        self.delegateObject.ServerCallFailed((response.result.error?.localizedDescription)!, name: name)
                                    }
                                }
        }
    }
    
    //MARK:- Multi part request with parameters.
    func requestMultiPartWithUrlAndParameters(_ urlString: String, parameters: [String : Any], fileParameterName: String, fileName:String, fileData :[UIImage], mimeType : String, delegate: ServerCallDelegate, name: ServerCallName) {
        
        self.delegateObject = delegate
        //--------------------------------------------------------
        //MARK:- COMMENT
        Alamofire.upload(
            multipartFormData: { multipartFormData in

                //The for loop is to append all parameters to multipart form data.
                for element in parameters.keys{
                    let strElement = String(element)
                    print(strElement)
                    
                    let strValueElement = parameters[strElement] as! String
                     print(strValueElement)
                    print(strValueElement.data(using: String.Encoding.utf8))

                    multipartFormData.append(strValueElement.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: strElement)
                }
                var ChangeNAme = 1
                for i in fileData {
                    let ImagDatatry = UIImageJPEGRepresentation(i, 0.2)
                    //Append file to multipart form data.
                    multipartFormData.append(ImagDatatry!, withName: fileParameterName, fileName: fileName +  String(ChangeNAme) , mimeType: mimeType)
                    
                    print(fileName)
                    
                    ChangeNAme = ChangeNAme + 1
                }
        },
            to: urlString,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        print(response.result.value!)
                        print(response.data as! NSData)
                        print("Success")
                        self.delegateObject.ServerCallSuccess(response.result.value! as AnyObject, name: name)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    print("fail")
                    self.delegateObject.ServerCallFailed(encodingError.localizedDescription, name: name)
                }
        }
        )
  
       //--------------------------------------------------------

       // --------------------------------------------------------
        //////////////////////////////////////

//        Alamofire.upload(multipartFormData: { multipartFormData in
//            // import image to request
//            for imageData in fileData {
//                multipartFormData.append(imageData, withName: fileParameterName + String(describing:imageData ), fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
//            }
//            for (key, value) in parameters {
//                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//            }
//        }, to: urlString,
//
//           encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                upload.responseJSON { response in
//                    print(response.result.value!)
//                                            //                        print(response.data as! NSData)
//
//                                            print("Success")
//                                            self.delegateObject.ServerCallSuccess(response.result.value! as AnyObject, name: name)
//
//                }
//            case .failure(let error):
//                print(error)
//
//                print(error)
//
//                                    print("fail")
//                                    self.delegateObject.ServerCallFailed(error.localizedDescription, name: name)
//            }
//
//        })
        /////////////
}
    
    /////////////////////////////////////////////////////////
        }


