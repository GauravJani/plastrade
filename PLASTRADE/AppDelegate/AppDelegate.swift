//
//  AppDelegate.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 07/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import ImageIO
import Google
import GoogleSignIn
import AssetsPickerViewController
import SDWebImage
import IQKeyboardManagerSwift
import RSLoadingView
import Cosmos
import CoreData
import GoogleMaps
import GooglePlacePicker
//import SkyFloatingLabelTextField



//import IQKeyboardManager
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    func setUpGoogleMaps() {
        let googleMapsApiKey = "AIzaSyA_OfTU9A3eI51gleESHJx2ygABPaZryXk"
        GMSServices.provideAPIKey(googleMapsApiKey)
        GMSPlacesClient.provideAPIKey("AIzaSyA_OfTU9A3eI51gleESHJx2ygABPaZryXk")
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if TypeOfGoogleAndFb == 3  {
        } else{
            return   GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
       
            setUpGoogleMaps()
            
            IQKeyboardManager.shared.enable = true
            
            GIDSignIn.sharedInstance().clientID =  "849585466724-96gc01vhlt9ooh5qr70c4jv41r9p2g34.apps.googleusercontent.com"
        }
        
        else{
        
            showAlert(title: "Internet Connection not Available!", message: "")
            print("Internet Connection not Available!")
        }
        
        
        
        
        
//        setUpGoogleMaps()
//
//        IQKeyboardManager.shared.enable = true
//
//
//        GIDSignIn.sharedInstance().clientID =  "849585466724-96gc01vhlt9ooh5qr70c4jv41r9p2g34.apps.googleusercontent.com"
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // FBSDKAppEvents.activateApp()
    }

    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

