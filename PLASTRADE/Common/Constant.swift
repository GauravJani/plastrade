//
//  Constant.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 22/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import Foundation
import UIKit
import RSLoadingView

let kEntetEmail:String               =    "Please Enter Email"
let kEnterValidEmail:String          =    "Please Enter Valid Email"
let kEnterMobileNumber:String        =    "Please Enter Mobile Number"


var viewShowLoad: UIView?



var TypeOfGoogleAndFb = 2

var Email_Type = 0

let _userDefault : UserDefaults = UserDefaults.standard


let kBasePath = "http://demo.saturncube.com/molding/api.php?action="


func AppColor()-> UIColor {
    
    return UIColor(red:0.00, green:0.21, blue:0.60, alpha: 1.0)
}

extension String {
    
    func isEmail() -> Bool {
        let regex = try? NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
        
        return regex?.firstMatch(in: self, options: [], range: NSMakeRange(0, self.characters.count)) != nil
    }
    
    func isStringWithoutSpace() -> Bool{
        return !self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
    }
   
    func contains(find: String) -> Bool{
        return self.range(of:find, options:String.CompareOptions.caseInsensitive) != nil
    }
    
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: .literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
}

func showAlert(title: NSString, message: String) {
    let obj = UIAlertView(title: title as String, message: message, delegate: nil, cancelButtonTitle: NSLocalizedString("Ok", comment: ""))
    obj.show()
}

/////////////
 func showOnWindow() {
    let loadingView = RSLoadingView()
    
    loadingView.mainColor = AppColor()
    
    loadingView.showOnKeyWindow()
}

func hideOnWindow() {
    RSLoadingView.hideFromKeyWindow()
}
////////////

//--------------------------------------------------------
//MARK:- Image Set In textField
func TextfieldSeetImageInRightSide(textField: UITextField,imageName: String) {
    
    textField.rightViewMode = UITextFieldViewMode.always
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    let image = UIImage(named: imageName)
    imageView.image = image
    textField.rightView = imageView
}


func TextfieldSeetImageInLeftSide(textField: UITextField,imageName: String) {
    
    textField.leftViewMode = UITextFieldViewMode.always
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    let image = UIImage(named: imageName)
    imageView.image = image
    textField.leftView = imageView
}
//--------------------------------------------------------
//--------------------------------------------------------
//MARK:- PushToOther VC

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

func SetIntialMainViewController(_ aStoryBoardID: String) {
    
    let mainVcObj = mainStoryboard.instantiateViewController(withIdentifier: aStoryBoardID)
    
    let navigationController : UINavigationController = UINavigationController(rootViewController: mainVcObj)
   
    navigationController.isNavigationBarHidden = true
    
}
//--------------------------------------------------------
