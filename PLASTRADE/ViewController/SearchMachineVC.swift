//
//  SearchMachineVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 16/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class SearchMachineVC: UIViewController,UISearchResultsUpdating{
    
    var a =  #imageLiteral(resourceName: "0 splash.jpg")
    
    // MArk:- Object of UISearchcontroller
    var resultSearchController = UISearchController()
    
    @IBOutlet weak var tbl_SearchTbl: UITableView!
    
    var tabledata = ["Ahmedabad","Kodinar","Rajkot","Una","Diu","Gujarat","India","Goa","Gurukul","Surat","Memnagar","Porbandar","Mehsana","Gandhinar","Veraval","Gir Somnath","Talala"]
    
    var filteredTableData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Mark:- Create UISearchController Search bar
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.barStyle = UIBarStyle.black
            controller.searchBar.barTintColor = UIColor.white
            controller.searchBar.backgroundColor = UIColor.clear
            self.tbl_SearchTbl.tableHeaderView = controller.searchBar
            return controller
        })()
        ////////////////////////////////////////
        self.tbl_SearchTbl.reloadData()
    }
    //----------------------------------------------------------------------
    
    //Mark:- Back Button
    @IBAction func btn_Back(_ sender: Any) {
        
    }
    //---------------------------------------
    @IBOutlet weak var txt_Search: UITextField!
    
    //--------------------------------------------------------
    //MARK:- UISEarch Controller Methods
    func updateSearchResults(for searchController: UISearchController) {
        
        filteredTableData.removeAll(keepingCapacity: true)
        print("filter data", filteredTableData.capacity);
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (tabledata as NSArray).filtered(using: searchPredicate)
        filteredTableData = array as! [String]
        self.tbl_SearchTbl.reloadData()
    }
    //--------------------------------------------------------
    
    
    
}
