//
//  PlasticInjectionMouldingVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 02/07/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker

class PlasticInjectionMouldingVC: UIViewController,GMSMapViewDelegate,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate {
    
    
    //--------------------------------------------------------
    //MARK:- UITextFields
    @IBOutlet weak var txt_LockingTonnage: UITextField!
    @IBOutlet weak var txt_ShotWeightInPE: UITextField!
    @IBOutlet weak var txt_TieBarFirst: UITextField!
    @IBOutlet weak var txt_TieBarSecond: UITextField!
    @IBOutlet weak var txt_MinMoldHeight: UITextField!
    @IBOutlet weak var txt_MaxDaylight: UITextField!
    @IBOutlet weak var txt_ElectricMotor: UITextField!
    @IBOutlet weak var txt_MoldStock: UITextField!
    @IBOutlet weak var txt_ConnectedLoad: UITextField!
    @IBOutlet weak var PlaceLocation: UITextField!
    
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_Back(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Next(_ sender: Any) {
    }
    
    //------------------------------------------------------------------------
    //MARK:- Google Address
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let MyController = GMSAutocompleteViewController()
        MyController.delegate = self
        self.present(MyController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress ?? "null")")
        self.PlaceLocation.text = place.formattedAddress
        print("Place attributions: \(String(describing: place.attributions))")
        
        self.dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        //        print("Error: \(error.description)")
        self.dismiss(animated: true, completion: nil)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        self.dismiss(animated: true, completion: nil)
    }
    //------------------------------------------------------------------------
    
    
}
