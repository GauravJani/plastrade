//
//  LoginVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 09/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import RSLoadingView

class LoginVC: UIViewController,ServerCallDelegate,UITextFieldDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Ot_CntAsGuest.backgroundColor = .clear
        Ot_CntAsGuest.layer.borderColor =  (AppColor().cgColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        txt_MobileNumber.text = ""
        txt_MobileNumber.keyboardType = UIKeyboardType.numberPad
        txt_CountryCode.keyboardType = UIKeyboardType.numberPad
    }
    
    //--------------------------------------------------------
    //MARK:- IBOutlet
    
    @IBOutlet weak var Ot_CntAsGuest: UIButton!
    //  @IBOutlet weak var Ot_SignUp: UIButton!
    
    @IBOutlet weak var txt_MobileNumber: UITextField!
    
    @IBOutlet weak var txt_CountryCode: UITextField!
    //@IBOutlet weak var txt_Password: UITextField!
    //@IBOutlet weak var txt_Password: UITextField!
    //--------------------------------------------------------
    
    @IBAction func btn_SignIn(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if isValidData() == false {
            
            return
        }
            
        else {
            self.view.endEditing(true)
            showOnWindow()
            self.CallApiForLogin()
        }
    }
    
    @IBAction func btn_CasGuest(_ sender: Any) {
        
        print("Click Done")
        let Hvcobj = self.storyboard?.instantiateViewController(withIdentifier: "HomeVCViewController") as! HomeVCViewController
        self.navigationController?.pushViewController(Hvcobj, animated: true)
    }
    
    // ---End ButtonS--- //
    
    // MARK: - Api Call Method
    func CallApiForLogin() {
        // init paramters Dictionary
        var myUrl = ""
        
        myUrl = kBasePath + "login"
        
        let param = [
            "mobile_no"    : txt_MobileNumber.text!
        ]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameLogin)
    }
    //--------------------------------------------------------
    //MARK:- Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        if name == ServerCallName.serverCallNameLogin {
            var dicData = resposeObject as! [AnyHashable : Any]
            if ((dicData["success"]) != nil) {
                
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                let strMsg     =  dicData["msg"]
                
                if strResponse == 1 {
                    print("Successfully login done")
                    _userDefault.set(true, forKey: "LoginUser")
                    
                    _userDefault.set(dicData["otp"],forKey: "OtpNumber")
                    _userDefault.set(dicData["mobile_no"], forKey: "MobileNumber")
                    //showAlert(title: "", message: "You have successfully Login")
                    hideOnWindow()
                    
                    let alertController = UIAlertController(title: strMsg as! String, message: dicData["otp"]! as! String, preferredStyle: .alert)
                    
                    let sendButton = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        
                        self.view.endEditing(true)
                        hideOnWindow()
                        
                        let VfmNbrObj = self.storyboard?.instantiateViewController(withIdentifier: "VerifyMobileNoVC") as! VerifyMobileNoVC
                        
                        self.navigationController?.pushViewController(VfmNbrObj, animated: true)
                    })
                    
                    alertController.addAction(sendButton)
                    self.present(alertController, animated: true, completion: nil)
                }
                else {
                    showAlert(title: "", message: "Email or Password is wrong")
                }
            }
        }
    }
    
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        hideOnWindow()
        showAlert(title: "", message: "Please Try Again")
        print("SomeTHingWrong ,ServerFailed")
    }
    
    //--------------------------------------------------------
    //  MARK: - Validation function
    func isValidData() -> Bool{
        if !(txt_MobileNumber?.text!.isStringWithoutSpace())!{
            
            showAlert(title: "", message: kEnterMobileNumber)
            
            return false
        }
            
        else if !(txt_CountryCode?.text!.isStringWithoutSpace())!{
            
            //showAlert(title: "", message: "Please Enter Enter Country Code")
            return false
        }
        
        return true
    }
    //--------------------------------------------------------
}
