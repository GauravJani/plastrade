//
//  StartScreenVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 30/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class StartScreenVC: UIViewController {
    
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if _userDefault.bool(forKey: "SKIP") == false {
            let Hvcobj = self.storyboard?.instantiateViewController(withIdentifier: "SliderVC") as! SliderVC
            self.navigationController?.pushViewController(Hvcobj, animated: true)
        }
        else {
            if _userDefault.bool(forKey: "LoginUser") == true {
                let Hvcobj = self.storyboard?.instantiateViewController(withIdentifier: "HomeVCViewController") as! HomeVCViewController
                self.navigationController?.pushViewController(Hvcobj, animated: true)
            }
            else {
                let LgnObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(LgnObj, animated: true)
            }
        }
    }
    
    
}
