//
//  PaytmPayScreenVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 19/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class PaytmPayScreenVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_debitCreditCard.isSelected = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_Back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_DebitCreditCard(_ sender: Any) {
        btn_debitCreditCard.isSelected = true
    }
    @IBAction func btn_PaytmPayMent(_ sender: UIControl) {
        btn_debitCreditCard.isSelected = false
    }
    @IBAction func btn_PayNow(_ sender: Any) {
    }
    
    @IBOutlet weak var txt_CardNumber: UITextField!
    @IBOutlet weak var txt_ExpiryDate: UITextField!
    @IBOutlet weak var txt_ExpiryYear: UITextField!
    @IBOutlet weak var text_CVV: UITextField!
    @IBOutlet weak var btn_debitCreditCard: UIButton!
    @IBOutlet weak var lbl_Rupee: UILabel!
    
}
