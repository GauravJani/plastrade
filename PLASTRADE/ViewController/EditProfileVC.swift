
import UIKit

class EditProfileVC: UIViewController,ServerCallDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var img_ProfileLogo: UIImageView!
    @IBOutlet weak var img_ImgView: UIImageView!
    var pic = UIImagePickerController()
    
    var ImgeURLString = ""
    var urlForImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pic.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Marl:- API Call For View 
        CallApiForViewProfileData()
    }
    
    var ImageArray = [UIImage]()
    //--------------------------------------------------------
    //MARK:- COMMENT
    @IBOutlet weak var btn_ProfileBTN: UIButton!
    @IBOutlet weak var txt_FirstName: UITextField!
    @IBOutlet weak var txt_LastName: UITextField!
    @IBOutlet weak var txt_MobileNumber: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    //--------------------------------------------------------
    
    @IBAction func btn_ProfileImageBTN(_ sender: Any) {
        pic.sourceType = .photoLibrary
        present(pic,animated: true,completion: nil)
    }
    
    @IBAction func btn_BackToMyProVC(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Save(_ sender: Any) {
        showOnWindow()
        CallApiForEditProfile()
        //        navigationController?.popViewController(animated: true)
    }
    
    //--------------------------------------------------------
    //MARK:- SertverMaethods
    
    //MARK: - Api Call Method
    func CallApiForEditProfile() {
        // showOnWindow()
        // init paramters Dictionary
        var myUrl = ""
        
        myUrl = kBasePath + "editProfile"
        
        let param = ["user_id"      :   "1",
                     "fname"        :   txt_FirstName.text!,
                     "lname"        :   txt_LastName.text!,
                     "mobile_no"    :   txt_MobileNumber.text!,
                     "email"        :   txt_Email.text!
            ]  as [String : Any]
        
        print(myUrl)
        
        ServerCall
            .sharedInstance
            .requestMultiPartWithUrlAndParameters(myUrl,
                                                  parameters: param as [String : AnyObject],
                                                  fileParameterName: "image",
                                                  fileName: "image.png",
                                                  fileData: ImageArray,
                                                  mimeType: "image/jpeg",
                                                  delegate: self ,
                                                  name: .serverCallNameUploadImage)
    }
    
    //Mark:- API Call For ViewData
    func CallApiForViewProfileData() {
        // showOnWindow()
        // init paramters Dictionary
        var myUrl = ""
        myUrl = kBasePath + "viewProfile"
        
        let param = ["user_id" : "1"] as [String : Any]
        
        print(myUrl)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameViewProFileData)
    }
    
    //MARK:- ServerCall Delegate Methods
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        //hideOnWindow()
        hideOnWindow()
        var dicData = resposeObject as! [AnyHashable : Any]
        if name == ServerCallName.serverCallNameEditProfile {
            
            if ((dicData["success"]) != nil) {
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                let strMsg     =  dicData["msg"]
                
                if strResponse == 1 {
                    let MobileNumberCheck =  dicData["change_number"] as! Int
                    
                    if MobileNumberCheck == 0 {
                        print(dicData["change_number"])
                        
                        var UserInfo = dicData["user_info"] as! [AnyHashable : Any]
                        ImgeURLString = UserInfo["profile_image"] as! String
                        print(ImgeURLString)
                        img_ProfileLogo.sd_setImage(with: URL(string : ImgeURLString))
                        
                        //img_ProfileLogo.image = img_ImgView.image
                        
                    }
                    if MobileNumberCheck == 1 {
                        
                        let Lvcobj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(Lvcobj, animated: true)
                        print(dicData["change_number"])
                    }
                    print("upload successfully")
                }
                else {
                    print("Not Upload Somthing Wrong")
                    // showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                hideOnWindow()
                print("comethe Else Part")
                //showAlert(title: "", message:strerrMsg)
            }
            hideOnWindow()
        }
        
        if name == ServerCallName.serverCallNameViewProFileData {
            
            hideOnWindow()
            if ((dicData["success"]) != nil) {
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                
                if strResponse == 1 {
                    let Result = dicData["result"] as? [AnyHashable : Any]
                    urlForImage =  Result!["profile_image"] as! String
                    img_ProfileLogo.sd_setImage(with: URL(string: urlForImage))
                    print(urlForImage)
                    print("upload successfully")
                }
                else {
                    print("Not Upload Somthing Wrong")
                    // showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                hideOnWindow()
                print("comethe Else Part")
                //showAlert(title: "", message:strerrMsg)
            }
        }
    }
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        hideOnWindow()
        print("Server Fail")
    }
    
    //--------------------------------------------------------
    //MARK:- Image Picker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if var picD = info[UIImagePickerControllerOriginalImage] as! UIImage?{
            img_ImgView.contentMode = .scaleAspectFit
            img_ImgView.image = picD
            img_ProfileLogo.image = img_ImgView.image
            ImageArray.append(img_ProfileLogo.image!)
        }
        dismiss(animated: true, completion: nil)
    }
}
