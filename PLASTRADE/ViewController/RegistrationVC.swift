//
//  RegistrationVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 09/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class  RegistrationVC: UIViewController,ServerCallDelegate {
    
    // Mark:- TextField
    @IBOutlet weak var txt_FirstName: UITextField!
    @IBOutlet weak var txt_LastName: UITextField!
    @IBOutlet weak var txt_MobileNumber: UITextField!
    @IBOutlet weak var txt_NewEmail: UITextField!
    
    //  @IBOutlet weak var btn_OutletBackArrow: UIButton!
    
    /////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _userDefault.set(false, forKey: "LoginUser")
        navigationController?.isToolbarHidden = true
        txt_MobileNumber.text = _userDefault.string(forKey: "MobileNumber")!
    }
    //************* Mark:- Button ************
    @IBAction func btn_SignUp(_ sender: Any) {
        
        self.view .endEditing(true)
        
        if isValidData() == false {
            print("Email not Valid")
            return
        }
        else {
            self.view.endEditing(true)
            // app_Delegate.startLoadingview("")
            showOnWindow()
            self.CallApiForLogin()
        }
    }
    
    //*************** End **********************
    //-------------------------------------------------------
    // MARK: - Api Call Method
    func CallApiForLogin() {
        // init paramters Dictionary
        var myUrl = ""
        
        myUrl = kBasePath + "register"
        
        let param = [
            "email"       :   txt_NewEmail.text!,
            "fname"       :   txt_FirstName.text!,
            "lname"       :   txt_LastName.text!,
            "mobile_no"   :   txt_MobileNumber.text!,
            
            ] as [String : Any]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameRegister)
    }
    //--------------------------------------------------------
    
    //--------------------------------------------------------
    // MARK: - Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameRegister {
            
            if ((dicData["success"]) != nil) {
                // app_Delegate.stopLoadingView()
                // Create the alert controller
                // let strResponse = TO_INT(dicData["success"]);
                let strMsg = dicData["msg"] as! String
                
                let strResponse = dicData["success"] as! Int
                let UserInfo = dicData["user_info"] as! [AnyHashable :Any]
                
                if strResponse == 1 {
                    
                    hideOnWindow()
                    print(_userDefault.string(forKey:"UserId"))
                    _userDefault.set(UserInfo["user_id"],forKey: "UserId")
                    print(_userDefault.string(forKey: "UserId"))
                    let alertController = UIAlertController(title: "", message: strMsg, preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        //                        _ = self.navigationController?.popViewController(animated: true)
                        let VMNoObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeVCViewController") as! HomeVCViewController
                        self.navigationController?.pushViewController(VMNoObj, animated: true)
                        NSLog("OK Pressed")
                    }
                    
                    // Add the actions
                    alertController.addAction(okAction)
                    
                    // Present the controller
                    self.present(alertController, animated: true, completion: nil)
                }
                else {
                    //  app_Delegate.stopLoadingView()
                    //  Constant.showAlert(title: "", message: strMsg)
                    hideOnWindow()
                    print("something is wrong")
                    return
                }
            }
            else {
                hideOnWindow()
                //let strerrMsg = TO_STRING(dicData["error"])
                //Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        // app_Delegate.stopLoadingView()
    }
    
    // MARK: - Server Failed Delegate
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        
        hideOnWindow()
        print("Somthing Wrong  Please Check it")
        //  app_Delegate.stopLoadingView()
        //   Constant.showAlert(title: "", message:errorObject)
    }
    
    //--------------------------------------------------------
    //  MARK: - Validation function
    func isValidData() -> Bool{
        
        if !(txt_FirstName?.text!.isStringWithoutSpace())!{
            
            showAlert(title: "", message: "Please Enter First Name")
            return false
        }
        else if !(txt_LastName?.text!.isStringWithoutSpace())!{
            
            showAlert(title: "", message: "Please Enter Last Name")
            return false
        }
            
        else if txt_MobileNumber.text?.count == 0 {
            
            showAlert(title: "", message:  kEnterMobileNumber)
            return false
        }
            
        else if !(txt_NewEmail?.text!.isStringWithoutSpace())!{
            
            showAlert(title: "", message: kEntetEmail )
            return false
        }
        else if !(txt_NewEmail.text!.isEmail()){
            
            showAlert(title: "", message:  kEnterValidEmail)
            return false
        }
        return true
    }
}
