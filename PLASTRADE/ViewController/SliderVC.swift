//
//  SliderVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 24/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class SliderVC: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var btnOutlet_StartBtn: UIButton!
    @IBOutlet weak var ScrlView: UIScrollView!
    
    var pageControl:UIPageControl = UIPageControl()
    
    @IBOutlet weak var srlvwCnstrnOtlt: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        btnOutlet_StartBtn.isHidden = true
        
        self.ScrlView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height - 45)
        let scrollViewWidth:CGFloat = self.ScrlView.frame.width
        let scrollViewHeight:CGFloat = self.ScrlView.frame.height
        
        let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgOne.image = UIImage(named: "WelComeSCreen")
        imgOne.contentMode = .scaleToFill
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "DiscoverScreen")
        imgTwo.contentMode = .scaleToFill
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "RealTImeScreen")
        imgThree.contentMode = .scaleToFill
        let imgFour = UIImageView(frame: CGRect(x:scrollViewWidth*3, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgFour.image = UIImage(named: "GenuineUserScreen")
        imgFour.contentMode = .scaleToFill
        
        self.ScrlView.addSubview(imgOne)
        self.ScrlView.addSubview(imgTwo)
        self.ScrlView.addSubview(imgThree)
        self.ScrlView.addSubview(imgFour)
        self.ScrlView.contentSize = CGSize(width:self.ScrlView.frame.width * 4, height:self.ScrlView.frame.height)
        self.ScrlView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        btnOutlet_StartBtn.isHidden = true
        // Change the text accordingly
        if Int(currentPage) == 3{
            btnOutlet_StartBtn.isHidden = false
        }
    }
    
    @IBAction func btn_Start(_ sender: Any) {
        
        _userDefault.set(true, forKey: "SKIP")
        let LgnObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(LgnObj, animated: true)
    }
}
