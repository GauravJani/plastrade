
import UIKit

class MyProfileVC: UIViewController,ServerCallDelegate {
    
    var imgView = UIImageView()
   
    @IBOutlet weak var img_MyProfileImage: UIImageView!
    @IBOutlet weak var lbl_MyName: UILabel!
    @IBOutlet weak var lbl_MyEmailID: UILabel!
    
    
    var urlForImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CallApiForViewProfileData()
    }
    
    //MARK:- Button  --------------------------------------------------------
    
    @IBAction func btn_Setting(_ sender: Any) {
        
        let StngVcObj = storyboard?.instantiateViewController(withIdentifier: "ChangePassWordVC") as! ChangePassWordVC
          self.navigationController?.pushViewController(StngVcObj, animated: true)
    }
    
    @IBAction func btn_AboutUS(_ sender: Any) {
        
    let AbtUsOBJ = self.storyboard?.instantiateViewController(withIdentifier: "PlstdVersionScreenVC") as! PlstdVersionScreenVC
        
        self.navigationController?.pushViewController(AbtUsOBJ, animated: true)
    }
    
    @IBAction func btn_ContactUs(_ sender: Any) {
     
        let ContcUsOBj = self.storyboard?.instantiateViewController(withIdentifier: "GetInTouchVC") as! GetInTouchVC
        self.navigationController?.pushViewController(ContcUsOBj, animated: true)
    }
    
    
    @IBAction func btn_ShareApp(_ sender: Any) {
        
        let activityViewController = UIActivityViewController(activityItems:
            [UIActivityType.airDrop], applicationActivities: nil)
        let excludeActivities = [
            UIActivityType.airDrop]
        activityViewController.excludedActivityTypes = excludeActivities;
        
        present(activityViewController, animated: true,
                completion: nil)
    }
    
    
    @IBAction func btn_RateUs(_ sender: Any) {
        
      let RateUsObj = self.storyboard?.instantiateViewController(withIdentifier: "RateUsVC") as! RateUsVC
        self.navigationController?.pushViewController(RateUsObj, animated: true)
        
    }
    
    @IBAction func btn_LogOut(_ sender: Any) {
        
        let BckLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(BckLoginVC, animated: true)
    }
    
    @IBAction func btn_EditProfile(_ sender: Any) {
        let EditProVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(EditProVC, animated: true)
    }
    
    @IBAction func btn_Home(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeVCViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
            else {
                let HomeVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "HomeVCViewController") as! HomeVCViewController
                
                self.navigationController?.pushViewController(HomeVcOBJ, animated: true)
                break
            }
        }
    }
    
    @IBAction func btn_Profile(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MyProfileVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
            else {
                let MyProVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                
                self.navigationController?.pushViewController(MyProVcOBJ, animated: true)
                break
            }
        }
    }
    
    @IBAction func btn_Sell(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MyItemVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
            else {
                let MyItemVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyItemVC") as! MyItemVC
                
                self.navigationController?.pushViewController(MyItemVcOBJ, animated: true)
                break
            }
        }
    }
    //--------------------------------------------------------
    //--------------------------------------------------------
    //MARK:-  SertverMaethods
   
    // MARK: - Api Call Method
    func CallApiForViewProfileData() {
        showOnWindow()
        
        // init paramters Dictionary
        var myUrl = ""
        myUrl = kBasePath + "viewProfile"
        let param = ["user_id" : "1"] as [String : Any]
        print(myUrl)
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameViewProFileData)
    }
    
    //MARK:- ServerCall Delegate Methods
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
       hideOnWindow()
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameViewProFileData {
            if ((dicData["success"]) != nil) {
            
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                
                if strResponse == 1 {
            
                    let Result = dicData["result"] as? [AnyHashable : Any]
                
                    lbl_MyName.text = Result!["fname"] as! String
                    lbl_MyEmailID.text = Result!["email"] as! String
                    
                    urlForImage =  Result!["profile_image"] as! String
                    
                    img_MyProfileImage.sd_setImage(with: URL(string: urlForImage))
                    print(urlForImage)
                    print("upload successfully")
                }
                else {
                
                    print("Not Upload Somthing Wrong")
                    // showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
            
                print("comethe Else Part")
                //showAlert(title: "", message:strerrMsg)
            }
           
        }
    }
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        hideOnWindow()
        
        print("Server Fail")
    }
    //--------------------------------------------------------

    
    
}
