//
//  VerifyMobileNoVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 09/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class VerifyMobileNoVC: UIViewController,ServerCallDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isToolbarHidden = true
    }
    
    var ServerTypeNo = 0
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        lbl_OTp.text = _userDefault.string(forKey:"MobileNumber")
        
        txt_First.keyboardType = UIKeyboardType.numberPad
        txt_Second.keyboardType = UIKeyboardType.numberPad
        txt_Third.keyboardType = UIKeyboardType.numberPad
        txt_Fourth.keyboardType = UIKeyboardType.numberPad
    }
    
    @IBOutlet weak var txt_First: UITextField!
    @IBOutlet weak var txt_Second: UITextField!
    @IBOutlet weak var txt_Third: UITextField!
    @IBOutlet weak var txt_Fourth: UITextField!
    @IBOutlet weak var lbl_OTp: UILabel!
    
    var OTP = ""
    
    @IBAction func btn_Change(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Resend(_ sender: Any) {
        
        txt_First.text = ""
        txt_Second.text = ""
        txt_Third.text = ""
        txt_Fourth.text = ""
        
        ServerTypeNo = 1
        self.view.endEditing(true)
        print("Click Done")
        showOnWindow()
        CallApiForLogin()
    }
    
    @IBAction func Verify(_ sender: Any) {
        self.view.endEditing(true)
        ServerTypeNo = 2
        showOnWindow()
        
        OTP = "\(txt_First.text!)" + "\(txt_Second.text!)" +  "\(txt_Third.text!)" + "\(txt_Fourth.text!)"
        
        
        print(_userDefault.string(forKey: "OtpNumber")!)
        if  3 >=  (OTP.count) {
            
            hideOnWindow()
            showAlert(title:"", message: "Please Enter OTP Number")
        }
            
        else if OTP  == _userDefault.string(forKey: "OtpNumber")!{
            print(OTP)
            showOnWindow()
            CallApiForLogin()
        }
        else if OTP  != _userDefault.string(forKey: "OtpNumber")! {
            print("Something Wrong")
            hideOnWindow()
            showAlert(title:"", message: "Please Enter Correct OTP")
            
        }
    }
    //    //MARK:- COMMENT------------------------------------------
    //    func textField(_ textField: UITextField,
    //                   shouldChangeCharactersIn range: NSRange,
    //                   replacementString string: String) -> Bool {
    //
    //        if ((textField.text?.count)! < 1) && (string.count > 0) {
    //
    //            if textField == txt_First {
    //                txt_Second.becomeFirstResponder()
    //            }
    //
    //            else if textField == txt_Second {
    //                txt_Third.becomeFirstResponder()
    //            }
    //
    //            else if textField == txt_Third {
    //                txt_Fourth.becomeFirstResponder()
    //            }
    //
    //            else if textField == txt_Fourth {
    //                txt_Fourth.resignFirstResponder()
    //            }
    //
    //            textField.text = string
    //
    //            return false
    //        }
    //
    //        else if ((textField.text?.count)! >= 1) && (string.count == 0) {
    //
    //            if textField == txt_Second {
    //                txt_Second.becomeFirstResponder()
    //            }
    //
    //            else if textField == txt_Third {
    //                txt_Third.becomeFirstResponder()
    //            }
    //
    //            else if textField == txt_Fourth {
    //
    //                print(txt_Fourth.text?.count)
    //
    //                print(string.count)
    //
    //                txt_Fourth.becomeFirstResponder()
    //            }
    //
    //            else if textField == txt_First {
    //                txt_First.resignFirstResponder()
    //            }
    //
    //            textField.text = ""
    //            return false
    //        }
    //        else if (textField.text?.count)! >= 1 {
    //
    //            textField.text = string
    //            return false
    //        }
    //
    //        return true
    //    }
    //--------------------------------------------------------
    // MARK: - Api Call Method
    func CallApiForLogin(){
        
        // init paramters Dictionary
        var myUrl = ""
        
        if ServerTypeNo == 1 {
            myUrl = kBasePath + "login"
            
            let param = [
                "mobile_no"    : _userDefault.string(forKey:"MobileNumber")!
            ]
            
            print(myUrl, param)
            
            ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameLogin)
        }
        
        if ServerTypeNo == 2 {
            
            myUrl = kBasePath + "verification_otp"
            
            let param = [
                "mobile_no"    : _userDefault.string(forKey:"MobileNumber")!,
                "otp"          : _userDefault.string(forKey: "OtpNumber")!]
            
            print(myUrl, param)
            
            ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameVerificationMobileNO)
        }
        
    }
    //------------------------------------------------------------
    
    //--------------------------------------------------------
    //MARK:- ServicCalling
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        
        print(resposeObject)
        if name == ServerCallName.serverCallNameVerificationMobileNO {
            var dicData = resposeObject as! [AnyHashable : Any]
            if ((dicData["success"]) != nil) {
                
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                //  let strMsg     =  dicData["msg"]
                
                print(strResponse)
                
                
                
                if strResponse == 1 {
                    print("Successfully login done")
                    
                    var UserRegisterResponceNumber  = dicData["user_registered"] as? Int
                    print(dicData["user_registered"])
                    
                    
                    _userDefault.set(dicData["otp"],forKey: "OtpNumber")
                    _userDefault.set(dicData["mobile_no"], forKey: "MobileNumber")
                    //showAlert(title: "", message: "You have successfully Login")
                    
                    if UserRegisterResponceNumber == 0  {
                        hideOnWindow()
                        print(UserRegisterResponceNumber)
                        
                        let VfmNbrObj = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
                        self.navigationController?.pushViewController(VfmNbrObj, animated: true)
                    }
                    if UserRegisterResponceNumber == 1{
                        hideOnWindow()
                        
                        let UserInfo = dicData["user_info"] as! [AnyHashable :Any]
                        _userDefault.set(UserInfo["user_id"],forKey: "UserId")
                        
                        print(_userDefault.string(forKey: "UserId"))
                        
                        let HmVObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeVCViewController") as! HomeVCViewController
                        
                        self.navigationController?.pushViewController(HmVObj, animated: true)
                    }
                }
                else {
                    hideOnWindow()
                    showAlert(title: "", message: "Email or Password is wrong")
                }
            }
        }
        
        ///////////////////
        if name == ServerCallName.serverCallNameLogin {
            var dicData = resposeObject as! [AnyHashable : Any]
            if ((dicData["success"]) != nil) {
                
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                let strMsg     =  dicData["msg"]
                
                if strResponse == 1 {
                    print("Successfully login done")
                    
                    _userDefault.set(dicData["otp"],forKey: "OtpNumber")
                    _userDefault.set(dicData["mobile_no"], forKey: "MobileNumber")
                    //showAlert(title: "", message: "You have successfully Login")
                    hideOnWindow()
                    let alertController = UIAlertController(title: dicData["otp"]! as! String, message: " is your new OTP", preferredStyle: .alert)
                    
                    let sendButton = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        
                        self.view.endEditing(true)
                        hideOnWindow()
                    })
                    
                    alertController.addAction(sendButton)
                    
                    self.navigationController!.present(alertController, animated: true, completion: nil)
                }
                else {
                    showAlert(title: "", message: "Email or Password is wrong")
                }
            }
        }
        ////////////////////
    }
    
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        hideOnWindow()
        print("SomeTHingWrong ,ServerFailed")
        
    }
    //--------------------------------------------------------
    
}
