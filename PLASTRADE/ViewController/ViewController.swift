//
//  ViewController.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 07/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    var  ax =  UIColor(red:0.00, green:0.21, blue:0.60, alpha:1.0).cgColor
    
    @IBOutlet weak var btn_SighUpOutlet: UIButton!
    
    @IBOutlet weak var btn_GuastLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        //Mark:- Set Border
        btn_SighUpOutlet.layer.borderWidth = 1
        //btn_SighUpOutlet.layer.borderColor = UIColor.blue.cgColo
        btn_SighUpOutlet.layer.borderColor = ax
        btn_GuastLogin.layer.borderWidth = 1
        btn_GuastLogin.layer.borderColor = ax
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

