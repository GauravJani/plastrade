//
//  PayTmPremiumScreenVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 19/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class PayTmPremiumScreenVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    //MArk:- Back Button
    @IBAction func btn_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var lbl_Rupee: UILabel!
    
    @IBOutlet weak var lbl_Details: UILabel!
    
    //Mark:- By now Button
    @IBAction func btn_BuyNow(_ sender: Any) {
        
        let PaymentVC = self.storyboard?.instantiateViewController(withIdentifier: "PaytmPayScreenVC") as! PaytmPayScreenVC
        self.navigationController?.pushViewController(PaymentVC, animated: true)
    }
    
    
}
