//
//  GetInTouchVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 10/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class GetInTouchVC: UIViewController,ServerCallDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var txt_Name: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_Mobile: UITextField!
    @IBOutlet weak var txt_Message: UITextView!
    
    @IBAction func btn_Submit(_ sender: Any) {
        
        self.view.endEditing(true)
        if isValidData() == false {
            print("Email not Valid")
            return
        }
        else {
            self.view.endEditing(true)
            // app_Delegate.startLoadingview("")
            showOnWindow()
            self.CallApiForGetInTouch()
        }
    }
    @IBAction func btn_Back(_ sender: Any) {
        self.view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    //--------------------------------------------------------
    //MARK:- Check Validation Function
    func isValidData() -> Bool{
        if !(txt_Name?.text!.isStringWithoutSpace())!{
            showAlert(title: "", message: "Please Enter First Name")
            return false
        }
        else if !(txt_Email?.text!.isStringWithoutSpace())!{
            showAlert(title: "", message: kEntetEmail )
            return false
        }
        else if !(txt_Email?.text!.isEmail())!{
            showAlert(title: "", message: kEnterValidEmail )
            return false
        }
        else if txt_Mobile.text?.count == 0 {
            showAlert(title: "", message:  kEnterMobileNumber)
            return false
        }
        else if !(txt_Message?.text!.isStringWithoutSpace())!{
            showAlert(title: "", message: "Please Enter Message")
            return false
        }
        return true
    }
    //--------------------------------------------------------
    // MARK: - Api Call Method
    func CallApiForGetInTouch() {
        
        showOnWindow()
        // init paramters Dictionary
        var myUrl = ""
        
        myUrl = kBasePath + "contactUs"
        
        let param = [
            "mobile_no"    :   txt_Mobile.text!,
            "name"         :   txt_Name.text!,
            "email"        :   txt_Email.text!,
            "message"      :   txt_Message.text!
        ]
        print(myUrl, param)
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameGetTouchWithUS)
    }
    //--------------------------------------------------------
    //MARK:- Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        
        hideOnWindow()
        if name == ServerCallName.serverCallNameGetTouchWithUS {
            
            var dicData = resposeObject as! [AnyHashable : Any]
            if ((dicData["success"]) != nil) {
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                let strMsg     =  dicData["msg"]
                
                if strResponse == 1 {
                    print("Successfully login done")
                    showAlert(title: strMsg as! NSString, message: "")
                }
                else {
                    showAlert(title: "Try Again", message: "")
                }
            }
        }
    }
    
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        hideOnWindow()
        showAlert(title: "", message: "Please Try Again")
        print("SomeTHingWrong ,ServerFailed")
    }
    
}

