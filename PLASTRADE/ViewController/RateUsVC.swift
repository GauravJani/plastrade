//
//  RateUsVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 25/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import Cosmos

class RateUsVC: UIViewController {
    
    var UserRatting = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cosmosViewOt.didFinishTouchingCosmos = {
            rating in
            print(rating)
            self.UserRatting = rating
        }
    }
    
    @IBOutlet weak var cosmosViewOt: CosmosView!
    
    @IBAction func Back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
