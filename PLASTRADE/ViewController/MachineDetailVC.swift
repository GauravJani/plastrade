//
//  MachineDetailVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 15/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import ImageSlideshow

class MachineDetailVC: UIViewController {
    
    @IBOutlet weak var Outlet_ImageViewSlider: ImageSlideshow!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Outlet_ImageViewSlider.setImageInputs([ImageSource(image: UIImage(named: "WelComeSCreen")!),ImageSource(image: UIImage(named: "DiscoverScreen")!)])
        
    }
    
    
}
