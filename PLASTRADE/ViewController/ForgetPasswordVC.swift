//
//  ForgetPasswordVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 24/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class ForgetPasswordVC: UIViewController,ServerCallDelegate,UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var txt_MailOrMobileNumber: UITextField!
    @IBAction func btn_Submit(_ sender: Any) {
        
        if isValidData() == false {
            return
        }
        else {
            self.view.endEditing(true)
            //        app_Delegate.startLoadingview("")
            self.CallApiForLogin()
        }
    }
    //////////////////////////////////////////////////
    //-------------------------------------------------------
    // MARK: - Api Call Method
    func CallApiForLogin() {
        // init paramters Dictionary
        var myUrl = ""
        
        myUrl = kBasePath + ""
        
        let param = [
            "email"    : txt_MailOrMobileNumber.text!
        ]
        
        print(myUrl, param)
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameLogin)
    }
    //--------------------------------------------------------
    
    //MARK:- Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        if name == ServerCallName.serverCallNameLogin {
            var dicData = resposeObject as! [AnyHashable : Any]
            if ((dicData["success"]) != nil) {
                
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                
                if strResponse == 1 {
                    
                    print("Successfully login done")
                    
                    let alertController = UIAlertController(title: "Password Send in your Mobile Number", message: "", preferredStyle: .alert)
                    
                    let sendButton = UIAlertAction(title: "Done", style: .default, handler: { (action) -> Void in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    
                    alertController.addAction(sendButton)
                    
                    self.navigationController!.present(alertController, animated: true, completion: nil)
                }
                else {
                    showAlert(title: "", message: "Mobile Number Are wrong")
                }
            }
        }
    }
    
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        print("SomeTHingWrong ,ServerFailed Server Faild")
    }
    //--------------------------------------------------------
    //  MARK: - Validation function
    func isValidData() -> Bool{
        if !(txt_MailOrMobileNumber?.text!.isStringWithoutSpace())!{
            showAlert(title: "", message: "Please Enter User Id" )
            return false
        }
        return true
    }
    //--------------------------------------------------------
    
}
