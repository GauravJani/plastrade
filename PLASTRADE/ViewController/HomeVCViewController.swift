//
//  HomeVCViewController.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 11/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import CoreLocation

class HomeVCViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Mark:- ClLocationManager
        
           locationManager.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
           locationManager.requestWhenInUseAuthorization()
           locationManager.startUpdatingLocation()
      
        //---------------------------------------
        
        //btn_Sell_Image.layer.cornerRadius = 25
        lbl_InjeCtionLabel.backgroundColor = UIColor.clear
        lbl_InjeCtionLabel.layer.backgroundColor = AppColor().cgColor
        lbl_BlowLabel.layer.backgroundColor = AppColor().cgColor
        lbl_BlowLabel.isHidden = true
        lbl_injectionLine.isHidden = true
        
        self.HomeCollectionViewOutlet.register(UINib(nibName: "HomeCollCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollCell")
    }
    
    //Mark :- Outlat //////////////
    @IBOutlet weak var lbl_injectionLine: UILabel!
    @IBOutlet weak var lbl_BlowLine: UILabel!
    @IBOutlet weak var HomeCollectionViewOutlet: UICollectionView!
    @IBOutlet weak var lbl_BlowLabel: UILabel!
    @IBOutlet weak var lbl_InjeCtionLabel: UILabel!
    @IBOutlet weak var btn_Sell_Image: UIButton!
    
    
    @IBOutlet weak var lbl_Location: UILabel!
    
    //*********END*********//
    
    
    //MARK:- Btn Actions-------------------------------------------------
    @IBAction func btn_MyProfile_Image(_ sender: Any) {
        print("Gaurav")
        let MyproVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        self.navigationController?.pushViewController(MyproVcOBJ, animated: true)
    }
    
    @IBAction func btn_Filter(_ sender: Any) {
        _userDefault.set(false, forKey: "LoginUser")
        print("Click")
        if let viewControllers = self.navigationController?.viewControllers {
            var isVcPresent = false
            for vc in viewControllers {
                // some process
                if vc.isKind(of:LoginVC.self) {
                    let FirstViewController = self.navigationController!.popToViewController(vc, animated: true)
                    break
                }
            }
        }
    }
    
    @IBAction func btn_Injection(_ sender: Any) {
        lbl_BlowLabel.isHidden = true
        lbl_injectionLine.isHidden = true
        lbl_InjeCtionLabel.isHidden = false
        lbl_BlowLine.isHidden = false
    }
    
    @IBAction func btn_Blow(_ sender: Any) {
        lbl_InjeCtionLabel.isHidden = true
        lbl_BlowLine.isHidden = true
        lbl_BlowLabel.isHidden = false
        lbl_injectionLine.isHidden = false
    }
    //
    //    @IBAction func btn_SellImg(_ sender: Any) {
    //        let MyItemVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyItemVC") as! MyItemVC
    //        self.navigationController?.pushViewController(MyItemVcOBJ, animated: true)
    //    }
    
    
    
    
    @IBAction func btn_ImageSell(_ sender: Any) {
        let MyItemVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyItemVC") as! MyItemVC
        self.navigationController?.pushViewController(MyItemVcOBJ, animated: true)
        
    }
    
    
    
    
    @IBAction func btn_Sell_Img(_ sender: Any) {
        
        //  let MyItemVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyItemVC") as! MyItemVC
        //        self.navigationController?.pushViewController(MyItemVcOBJ, animated: true)
    }
    
    //**********End***************//
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : HomeCollCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollCell", for: indexPath) as! HomeCollCell
        
        cell.HEllo.text = "ProcutName"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (HomeCollectionViewOutlet.frame.size.width / 2 - 26)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
    }
    
//--------------------------------------------------------
    
////MARK:- Get User Location--------------------------------------------------------
//    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
//            
//            if (error != nil) {
//                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
//                return
//            }
//            
//            if (placemarks?.count)! > 0 {
//                let pm = placemarks?[0]
//                self.displayLocationInfo(pm)
//            } else {
//                print("Problem with the data received from geocoder")
//            }
//        })
//    }
//    
//    func displayLocationInfo(_ placemark: CLPlacemark?) {
//        if let containsPlacemark = placemark {
//            //stop updating location to save battery life
//            locationManager.stopUpdatingLocation()
//            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
//            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
//            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
//            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
//            
////            localityTxtField.text = locality
////            postalCodeTxtField.text = postalCode
////            aAreaTxtField.text = administrativeArea
////            countryTxtField.text = country
//        }
//        
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("Error while updating location " + error.localizedDescription)
//    }
////--------------------------------------------------------

    
    
}

////////////////////////////////////////////

