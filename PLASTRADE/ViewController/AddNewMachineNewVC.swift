//
//  AddNewMachineNewVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 02/07/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class AddNewMachineNewVC: UIViewController,ServerCallDelegate {
    
    var CheckTextField = 0
    var currentTextField: UITextField?
    
    var MainDataType                      =   [String]()
    var SeleceCategeryForInjection        =   [String]()
    var SeleceCategeryForBlow             =   [String]()
    var PlasticInjectionMachine           =   [String]()
    var PetStretchBlowMouldingMachinetype =   [String]()
    var PetBlowMouldingMachinetype        =   [String]()
    var DataFinal                         =   [String]()
    
    //--------------------------------------------------------
    //MARK:- All Object
    
    //Datepicker Object
    let datePicker = UIDatePicker()
    
    //UiPicjkerView Object
    var pickerView = UIPickerView()
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MArk:- object of dalegat Pickerview
        pickerView.delegate = self
        
        
        //Mark:- UIDate Picker Function Call
        showDatePicker()
        
        CallApiForLogin()
    }
    
    //--------------------------------------------------------
    //MARK:- Buttons
    @IBAction func btn_Back(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Next(_ sender: Any) {
        
        TexfieldDartaValidationFun()
    }
    //-------------------------------------------------------------
    //mark:- TextField---------------------------------------------
    @IBOutlet weak var txt_SelectMouldingType: UITextField!
    @IBOutlet weak var txt_SelectMouldingCategory: UITextField!
    
    //  @IBOutlet weak var txt_SelectMachineType: UITextField!
    
    @IBOutlet weak var txt_CompanyName: UITextView!
    @IBOutlet weak var txt_Model: UITextView!
    @IBOutlet weak var txt_ManufactureYear: UITextField!
    @IBOutlet weak var txt_Price: UITextField!
    @IBOutlet weak var txt_MachineType: UITextField!
    
    //----------------------------------------------------------------
    //Mark:- Date Picker
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txt_ManufactureYear.inputAccessoryView = toolbar
        txt_ManufactureYear.inputView = datePicker
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        txt_ManufactureYear.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    //---------------------------------------------------------------------------
    
    //--------------------------------------------------------
    //MARK:- API Calling For Get MAchine Type
    
    func CallApiForLogin() {
        showOnWindow()
        // init paramters Dictionary
        var myUrl = ""
        myUrl = kBasePath + "getMachineTypes"
        print(myUrl)
        
        ServerCall.sharedInstance.requestWithURL(.GET, urlString: myUrl, delegate: self, name:.serverCallNameGetInjectionModulMachineName )
    }
    
    //--------------------------------------------------------
    
    
    //--------------------------------------------------------
    //MARK:- Service Call delegat Methods
    
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        
        hideOnWindow()
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if ((dicData["success"]) != nil) {
            
            // Create the alert controller
            let strResponse = dicData["success"] as? Int
            let strMsg     =  dicData["msg"]
            
            if strResponse == 1 {
                
                ApiResponceFun(ApiResponce: resposeObject)
                //  MainLogin.init(fromJson: )
                print("Successfully login done")
            }
            else {
                print("Wrong")
                // showAlert(title: "", message: "Email or Password is wrong")
            }
        }
    }
    
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        
        print("Something Wrong")
        hideOnWindow()
    }
    //--------------------------------------------------------
    
    //MArk:-  func Call When response get by Server
    func ApiResponceFun(ApiResponce: Any) {
        
        print(ApiResponce)
        
        //Mark:- Crate OBJ Of Main response class And call constructor
        let NextObj =  RootClass.init(fromJson: ApiResponce as! [AnyHashable : Any])
        
        //Mark:- Get Data from response Class
        for i in NextObj.result{
            
            for jp  in i.subcat {
                
                for q in jp.innerSubType.machine {
                    print(q.typeName)
                }
            }
            if i.subcat.count == 1 {
                print(i.subcat)
                for j  in i.subcat {
                    
                    for l in j.innerSubType.machine {
                        
                        print(l.typeName)
                        PlasticInjectionMachine.append(l.typeName)
                    }
                    
                    print(j.subType)
                    SeleceCategeryForInjection.append(j.subType)
                }
            }
            
            if  i.subcat.count == 2 {
                
                print(i.subcat[1].subType)
                var CheckMachineType = 0
                for K in i.subcat {
                    
                    for P in K.innerSubType.machine {
                        if CheckMachineType == 0{
                            print(P.typeName)
                            PetStretchBlowMouldingMachinetype.append(P.typeName)
                        }
                        else{
                            print(P.typeName)
                            PetBlowMouldingMachinetype.append(P.typeName)
                        }
                        
                    }
                    CheckMachineType = 1
                    print(K.subType)
                    SeleceCategeryForBlow.append(K.subType)
                }
            }
            
            MainDataType.append(i.mainType)
        }
    }
    
    //----------------------------------------------------------------------------
    //MARK:- VAlidation Functiuon
    func TexfieldDartaValidationFun() {
        
        if txt_SelectMouldingType.text == "Select Moulding Type" {
            showAlert(title: "Select Molding Type", message: "OK")
        }
            
        else if txt_SelectMouldingCategory.text == "Select Moulding Category" {
            showAlert(title: "Select Moulding Category", message: "OK")
        }
            
        else if txt_MachineType.text == "Select Machine Type" {
            showAlert(title: "Select Machine Type", message: "Ok")
        }
            
        else if txt_CompanyName.text == "" {
            showAlert(title: "Please Enter Company Name", message: "OK")
        }
        else if txt_Model.text == "" {
            showAlert(title: "Please Enter Model", message: "OK")
        }
        else if txt_ManufactureYear.text == "Select Manufacture Year" {
            showAlert(title: "Please Enter Manufacture Year", message: "Ok")
        }
        else if txt_Price.text == "" {
            showAlert(title: "Please Enter Price", message: "OK")
        }
        else {
            PushToNextVCFun()
        }
    }
    //----------------------------------------------------------------------------
    
    func PushToNextVCFun() {
        
        if txt_SelectMouldingCategory.text == "Plastic Injection Moulding" {

            
            SetIntialMainViewController("PlasticInjectionMouldingVC")
            
            
            
            
//            let Obj = self.storyboard?.instantiateViewController(withIdentifier: "PlasticInjectionMouldingVC") as! PlasticInjectionMouldingVC
//
//            self.navigationController?.pushViewController(Obj, animated: true)
            
        }
        
        if txt_SelectMouldingCategory.text == "Pet Stretch Blow Moulding" {
            let Obj = self.storyboard?.instantiateViewController(withIdentifier: "PlasticBlowMoulding") as! PlasticBlowMoulding
            
            self.navigationController?.pushViewController(Obj, animated: true)
        }
        
        if txt_SelectMouldingCategory.text == "Plastic Blow Moulding" {
            let Obj = self.storyboard?.instantiateViewController(withIdentifier: "PetStretchBlow") as! PetStretchBlow
            
            self.navigationController?.pushViewController(Obj, animated: true)
        }
    }
}
