//
//  PetStretchBlow.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 02/07/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class PetStretchBlow: UIViewController {
    
    
    //--------------------------------------------------------
    //MARK:- UITextField
    @IBOutlet weak var txt_ClampingFource: UITextView!
    @IBOutlet weak var txt_MachineCapacity: UITextView!
    @IBOutlet weak var txt_MaxNeckSize: UITextView!
    @IBOutlet weak var txt_PumpMotor: UITextView!
    @IBOutlet weak var txt_HeatingLoad: UITextView!
    @IBOutlet weak var txt_ConnectedLoad: UITextView!
    @IBOutlet weak var txt_PlaceLocation: UITextField!
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btn_Back(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Next(_ sender: Any) {
    }
    
    
    
    
}
