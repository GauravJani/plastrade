//
//  FilterVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 04/06/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {
    
    //MARK:- VC Methods --------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_BlowDetail.isHidden = true
        TextfieldSeetImageInRightSide(textField: txt_State, imageName:"DropDwn")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btn_Injection_OutLet.isSelected = true
    }
    
    //-------------------------------------------------------------------------
    
    //MARK:- Outlet-------------------------------------------
    @IBOutlet weak var txt_State: UITextField!
    @IBOutlet weak var view_BlowDetail: UIView!
    @IBOutlet weak var Outlet_ScrolView: UIScrollView!
    @IBOutlet weak var btn_Injection_OutLet: UIButton!
    @IBOutlet weak var btn_Blow_OutLet: UIButton!
    //--------------------------------------------------------
    
    //MARK:- BTN ACTION --------------------------------------------------------
    @IBAction func btn_Injection(_ sender: Any) {
        view_BlowDetail.isHidden = true
        btn_Injection_OutLet.isSelected = true
        btn_Blow_OutLet.isSelected = false
    }
    
    @IBAction func btn_Blow(_ sender: Any) {
        view_BlowDetail.isHidden = false
        btn_Injection_OutLet.isSelected = false
        btn_Blow_OutLet.isSelected = true
        
    }
    //--------------------------------------------------------------------------
    
}
