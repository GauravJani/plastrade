//
//  MyItemVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 18/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.

import UIKit

class MyItemVC: UIViewController {
    
    //,UITableViewDelegate,UITableViewDataSource
    var ItemType = 0
    
    @IBOutlet weak var tblOutlet: UITableView!

    @IBOutlet weak var btn_SwichbtnOutlet: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        lbl_SoldItemlabel.isHidden = true
        lbl_SoldItemLininglbl.isHidden = false
        lbl_NewItemLininglbl.isHidden = true
        lbl_NewItemLabel.isHidden = false
    }
    
    
    //--------------------------------------------------------
    //MARK:- UI label
    @IBOutlet weak var lbl_SoldItemLininglbl: UILabel!
    @IBOutlet weak var lbl_NewItemLininglbl: UILabel!
    @IBOutlet weak var lbl_NewItemLabel: UILabel!
    @IBOutlet weak var lbl_SoldItemlabel: UILabel!
    //--------------------------------------------------------
    
  
    //MARK:- UI BUTTONS
    
    @IBAction func btn_AddNewItem(_ sender: Any) {
    
        let AddNewItemobj = self.storyboard?.instantiateViewController(withIdentifier: "AddNewMachineNewVC") as! AddNewMachineNewVC
        
        self.navigationController?.pushViewController(AddNewItemobj, animated: true)
    }
    
    @IBAction func btn_SoldItem(_ sender: Any) {
        ItemType = 1
        lbl_NewItemLabel.isHidden = true
        lbl_NewItemLininglbl.isHidden = false
        lbl_SoldItemlabel.isHidden = false
        lbl_SoldItemLininglbl.isEnabled = true
        tblOutlet.reloadData()
    }

    @IBAction func btn_NewItem(_ sender: Any) {
    
        ItemType = 0
        lbl_SoldItemlabel.isHidden = true
        lbl_NewItemLabel.isHidden = false
        lbl_NewItemLininglbl.isHidden = true
        lbl_SoldItemLininglbl.isHidden = false
       
        tblOutlet.reloadData()
    }
    
    @IBAction func btn_Home(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeVCViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
            else {
                let HomeVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "HomeVCViewController") as! HomeVCViewController
                
                self.navigationController?.pushViewController(HomeVcOBJ, animated: true)
                break
            }
        }
    }
    
    @IBAction func btn_Profile(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeVCViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
            else {
                let ProVcOBJ = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                
                self.navigationController?.pushViewController(ProVcOBJ, animated: true)
                break
            }
        }
    }
    //----------------------------------------------------------
}
