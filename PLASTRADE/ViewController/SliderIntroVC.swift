//
//  SliderIntroVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 12/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import ImageSlideshow

class SliderIntroVC: UIViewController,UIScrollViewDelegate{
    
    @IBOutlet weak var LayoutSlider: NSLayoutConstraint!
    @IBOutlet weak var slideshow: ImageSlideshow!
    
    
    var pageControl:UIPageControl = UIPageControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var currentPage: ImageSlideshow = ImageSlideshow()
        
        LayoutSlider.constant = 0
        slideshow.circular = false
        
        slideshow.setImageInputs([
            ImageSource(image: UIImage(named: "WelComeSCreen")!),
            ImageSource(image: UIImage(named: "DiscoverScreen")!),
            ImageSource(image: UIImage(named: "RealTImeScreen")!),
            ImageSource(image: UIImage(named: "GenuineUserScreen")!),
            ])
    }
    
    //=============================================
    //MARK:- ScrollView Delegate
    //=============================================
    
    
}
