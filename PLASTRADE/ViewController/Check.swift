//
//  Check.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 17/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
class Check: UIViewController {

    @IBOutlet weak var swich: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        View_BlowView.isHidden = true
        btn_InjectionOTLT.isSelected  = true
        View_BlwoFinal.isHidden = true
    }

    
    @IBOutlet weak var View_BlwoFinal: UIView!
    @IBOutlet weak var View_BlowView: UILabel!
    @IBOutlet weak var btn_BlwoOtlT: UIButton!
    
    @IBAction func btn_BLow(_ sender: Any) {
        
        btn_InjectionOTLT.isSelected = false
        btn_BlwoOtlT.isSelected = true
        
        View_BlwoFinal.isHidden = false
    }
    
    @IBOutlet weak var btn_InjectionOTLT: UIButton!
    
    @IBAction func btnInjectionVC(_ sender: Any) {
        View_BlwoFinal.isHidden = true
       btn_InjectionOTLT.isSelected = true
        btn_BlwoOtlT.isSelected = false
    }
}
