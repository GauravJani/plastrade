//  AddNewItemVC.swift
//  PLASTRADE
//
//  Created by Gaurang Mistry on 21/05/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//
import UIKit
import AssetsLibrary
import AssetsPickerViewController
import Photos
import TinyLog
import MapKit
import CoreLocation

//class AddNewItemVC: UIViewController,ServerCallDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AssetsPickerViewControllerDelegate,CLLocationManagerDelegate {

class AddNewItemVC: UIViewController,ServerCallDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AssetsPickerViewControllerDelegate,CLLocationManagerDelegate {

    var CurrentLatitude  =  "0"
    var CurrentLongitude =  "0"
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
       let location  = locations.last
        print(location?.coordinate.latitude)
        print(location?.coordinate.longitude)
        
        CurrentLongitude = String(describing: location?.coordinate.longitude)
        CurrentLatitude = String(describing: location?.coordinate.latitude)
        
         print(CurrentLongitude)
         print(CurrentLatitude)
    
    }
    
    let locationManager = CLLocationManager()
    //--------------------------------------------------------
    //MARK:- Multiple Image Convert Code
    let imageManager = PHImageManager.default()
    let requestOptions = PHImageRequestOptions()
    let fetchOptions = PHFetchOptions()
    //--------------------------------------------------------
    
    var MouldingType = 0
    var MachineType = 0
    var UpdateType = 0

    @IBOutlet weak var imgMyImage: UIImageView!
    
    
    //MARK:- Image Picker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if var pic = info[UIImagePickerControllerOriginalImage] as! UIImage?{
            
            imgMyImage.contentMode = .scaleAspectFit
            imgMyImage.image = pic
            ImageArray.append(imgMyImage.image!)
        }
        dismiss(animated: true, completion: nil)
    }
    
    var pic = UIImagePickerController()
    
    var ImageArray = [UIImage]()
    var ChecktextField = 0
    //--------------------------------------------------------
    //MARK:- pickerView Programticallu
    // UIPickerView.
    private var myUIPicker: UIPickerView!
    private var MyUiPickerForSubDetail: UIPickerView!
    @IBOutlet weak var sbUIPicker: UIPickerView?
    //--------------------------------------------------------
    //--------------------------------------------------------
    //MARK:- Server Arry And Var
    var MachineTypesNAme = [String]()
    var MachineSubetypeName1 = [String]()
    var MachineSubetype1  = [Int]()
    var SubIDAll = 0
    var MachineSubetypeName2 = [String]()
    var MachineSubetype2 = [String]()
    //--------------------------------------------------------

    //MARK:- Outlet------------------------------------------
    @IBOutlet weak var Otlet_BrokerImage: UIButton!
    @IBOutlet weak var Outlet_CustomerImage: UIButton!
    @IBOutlet weak var lbl_MainMachineName: UITextField!
    //--------------------------------------------------------
    
    //MARK:- textfield---------------------------------------
    @IBOutlet weak var txt_PriceNew: UITextField!
    @IBOutlet weak var txt_CompanyName: UITextField!
    @IBOutlet weak var txt_pasticModulMachinename: UITextField!
    @IBOutlet weak var txt_TypeOfMachine: UITextField!
    @IBOutlet weak var txt_Make: UITextField!
    @IBOutlet weak var txt_ManuFYear: UITextField!
    @IBOutlet weak var txt_Model: UITextField!
    @IBOutlet weak var txt_LockingTonnage: UITextField!
    @IBOutlet weak var txt_ShotWeight: UITextField!
    @IBOutlet weak var TieBarDi1: UITextField!
    @IBOutlet weak var txt_TieBarDis2: UITextField!
    @IBOutlet weak var txt_MinMouldHeight: UITextField!
    @IBOutlet weak var txt_MaxMouldHeight: UITextField!
    @IBOutlet weak var txtElectricalMotor: UITextField!
    @IBOutlet weak var txt_MoldStock: UITextField!
    @IBOutlet weak var txt_ConnectedLoad: UITextField!
    @IBOutlet weak var txt_PlaceLocation: UITextField!
    @IBOutlet weak var txt_PersonName: UITextField!
    @IBOutlet weak var txt_ContectAddress: UITextField!
    @IBOutlet weak var txt_PhoneNo: UITextField!
    @IBOutlet weak var txt_EmailID: UITextField!
    @IBOutlet weak var txt_WhatAppNo: UITextField!
    //--------------------------------------------------------
    
    ////////////////////////////////
    @objc func PickerDoneBtn(sender: UIBarButtonItem) {
        if ChecktextField == 1 {
             txt_pasticModulMachinename.resignFirstResponder()
        }
        if ChecktextField == 2{
             txt_TypeOfMachine.resignFirstResponder()
        }
    }
    
    //--------------------------------------------------------
    //MARK:- AssetsPickerViewController Mathods
    func assetsPickerCannotAccessPhotoLibrary(controller: AssetsPickerViewController) {
        print("1")
    }
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {
        print("2")
        
    }
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        
        if assets.count > 3  {
        }
        else{
        }
        // do your job with selected assets
    }
    //--------------------------------------------------------
    func assetsPicker(controller: AssetsPickerViewController, didSelect asset: PHAsset, at indexPath: IndexPath) {print("1")}
    func assetsPicker(controller: AssetsPickerViewController, shouldDeselect asset: PHAsset, at indexPath: IndexPath) -> Bool {
    print("4")
        return true
    }
    func assetsPicker(controller: AssetsPickerViewController, didDeselect asset: PHAsset, at indexPath: IndexPath) {print("1")}
    //--------------------------------------------------------

    var ak = 0
    //MARK:- To limit selected assets count,
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
      print("5")
       
        if ak > 3 {
            // do your job here
        return false
        }
        return true
    }
    
    //--------------------------------------------------------
    //MARK:- ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
  
            pic.delegate = self
        //--------------------------------------------------------
        //MARK:- COMMENT
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true

        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target:self, action:#selector(PickerDoneBtn(sender:)))

        let spacebtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
       let cancelButton = UIBarButtonItem(title: "Cancle", style: UIBarButtonItemStyle.plain, target:self, action:#selector(PickerDoneBtn(sender:)))

        toolBar.setItems([cancelButton, spacebtn, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        txt_pasticModulMachinename.inputAccessoryView = toolBar
        txt_TypeOfMachine.inputAccessoryView = toolBar
        //-------------------------------------------------------
        
        txt_TypeOfMachine.delegate = self
        //--------------------------------------------------------

        //--------------------------------------------------------
        //MARK:- PickrView Code
        myUIPicker = UIPickerView()
        MyUiPickerForSubDetail = UIPickerView()
        
        // set delegate
        myUIPicker.delegate = self
        MyUiPickerForSubDetail.delegate = self
        
        // set DataSource
        myUIPicker.dataSource = self
        MyUiPickerForSubDetail.dataSource = self
        
        // add it to view
        //self.view.addSubview(myUIPicker)
        
        txt_pasticModulMachinename.inputView = myUIPicker
        txt_TypeOfMachine.inputView = MyUiPickerForSubDetail
        //--------------------------------------------------------
        
        //--------------------------------------------------------
  
        TextfieldSeetImageInRightSide(textField: txt_pasticModulMachinename, imageName:"DropDwn")
        TextfieldSeetImageInRightSide(textField: txt_TypeOfMachine, imageName:"DropDwn")
        TextfieldSeetImageInRightSide(textField: txt_ManuFYear, imageName:"DropDwn")
        Otlet_BrokerImage.isSelected = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CallApiForLogin()
    }
    //--------------------------------------------------------

    //MARK:- Button Action--------------------
    
    @IBAction func btn_BrokerImage(_ sender: Any) {
    Otlet_BrokerImage.isSelected = true
    Outlet_CustomerImage.isSelected = false
    UpdateType = 1
    }
    
    @IBAction func btn_CustomerImage(_ sender: Any) {
    Otlet_BrokerImage.isSelected = false
    Outlet_CustomerImage.isSelected = true
    UpdateType = 2
    }

    @IBAction func btn_Back(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_MachineNameDropDwn(_ sender: Any) {
    }
    
    //Mark:- Button Click Image From Galary
    @IBAction func btn_AttachmentImage(_ sender: Any) {
        pic.sourceType = .photoLibrary
        present(pic,animated: true,completion: nil)
    }

    @IBAction func btn_SubMit(_ sender: Any) {

     let PaytmVcObj = self.storyboard?.instantiateViewController(withIdentifier: "PayTmPremiumScreenVC") as! PayTmPremiumScreenVC
     self.navigationController?.pushViewController(PaytmVcObj, animated: true)
        
        if txt_pasticModulMachinename.text == "Plastic Injection Moulding"{
            MouldingType = 3
            MachineType = 1
        }
        else if txt_pasticModulMachinename.text == "Pet Stretch Blow Moulding"{
            MouldingType = 4
            MachineType = 5
        }
        else if txt_pasticModulMachinename.text == "Plastic Blow Moulding" {
            MouldingType = 5
            MachineType = 9
        }
         //ApiCallForUpdateProfileImage()
         CallApiForImageUpload()
    }
    
    //--------------------------------------------------------
    // MARK: - Api Call Method
    func CallApiForLogin() {
        showOnWindow()
        // init paramters Dictionary
        var myUrl = ""
        myUrl = kBasePath + "getMachineTypes"
        print(myUrl)
        
        ServerCall.sharedInstance.requestWithURL(.GET, urlString: myUrl, delegate: self, name:.serverCallNameGetInjectionModulMachineName )
    }
    
    //MARK:- ServerCall Delegate Methods
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
    
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameUploadImage {
            
            hideOnWindow()
            if ((dicData["success"]) != nil) {
                 hideOnWindow()
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                let strMsg     =  dicData["msg"]
                
                if strResponse == 1 {
                    print("upload successfully")
                }
                else {
                   
                    print("Not Upload Somthing Wrong")
                    
                   // showAlert(title: "", message: strMsg)
                    return
                }
            }
                
            else {
                 hideOnWindow()
                print("comethe Else Part")
                 //showAlert(title: "", message:strerrMsg)
            }
               hideOnWindow()
        }
        
        if name == ServerCallName.serverCallNameGetInjectionModulMachineName {
           
            if ((dicData["success"]) != nil) {
                
                // Create the alert controller
                let strResponse = dicData["success"] as? Int
                let strMsg     =  dicData["msg"]
                
                if strResponse == 1 {
                    print("Successfully login done")
                    hideOnWindow()
                    self.view.endEditing(true)
              var MainArray = dicData["result"] as! NSArray
              var MainArrayDic = MainArray as! NSArray
                  
              //MARK:- Array[0] potion Data --------------------------
              var SubArray0 = MainArrayDic[0] as! [AnyHashable : Any]
              var SubCatArray0 = SubArray0["subcat"] as! NSArray
              var AtyType      = SubCatArray0[0] as! [AnyHashable :Any]
               
                    var SubDetailsDic =  AtyType["inner_sub_type"]! as! [AnyHashable : Any]
                    
                    let SubDetailArry = SubDetailsDic["machine"] as! NSArray
                    
                    for i in SubDetailArry{
                    var SubdetailShow = i as! [AnyHashable: Any]
                   MachineSubetypeName1.append(SubdetailShow["type_name"]! as! String)
                   MachineSubetype1.append(SubdetailShow["id"]! as! Int)
                    }
                   MachineTypesNAme.append(AtyType["sub_type"]! as! String)
               //--------------------------------------------------------
               //MARK:- Array[2] potion Data --------------------------
               var SubArraay1 = MainArrayDic[1] as! [AnyHashable :Any]
               var SubCatArray1 = SubArraay1["subcat"] as! NSArray
               var AtyType2 = SubCatArray1 as! NSArray
               var CheckArrayNumber = 0
                
                    for i in AtyType2 {
                       
                        var SubTypesAry2 = i as! [AnyHashable :Any]
                        MachineTypesNAme.append(SubTypesAry2["sub_type"] as! String)
                        print(SubTypesAry2["sub_type"])
                       // SubIDAll = SubTypesAry2["sub_id"]! as! Int
                        
                        if CheckArrayNumber == 0 {
                        //SubIDAll = SubTypesAry2["sub_id"]! as! Int
                            
              var Innertype = SubTypesAry2["inner_sub_type"] as! [AnyHashable :Any]
                    
                       var  PetStretchBlowType = Innertype["machine"] as! NSArray
                    
                        for j in PetStretchBlowType{
                            print(j)
                        }
                        
                      var  PetStretchBlowMachine = Innertype["type"] as! NSArray
                        
                        for k in PetStretchBlowMachine{
                            
                            print(k)
                        }
                    }
                        if CheckArrayNumber == 1 {
                            
                          //  SubIDAll = SubTypesAry2["sub_id"]! as! Int
                            
                            var InnertypeTyp2 = SubTypesAry2["inner_sub_type"] as! [AnyHashable :Any]
                            
                            var  PlasticBlowMachine = InnertypeTyp2["machine"] as! NSArray
                            
                            for j in  PlasticBlowMachine{
                                
                                print(j)
                            }
                            
                            var PlasticBlowStage = InnertypeTyp2["stage"] as! NSArray
                            for k in PlasticBlowStage{

                                print(k)
                            }
                            var PlasticBlowClampingUnit = InnertypeTyp2["unit"] as! NSArray
                            
                            for k in PlasticBlowClampingUnit{
                                
                                print(k)
                            }
                        }
                        "plastic_blow_clamping_unit"
                        CheckArrayNumber = 1
                }
                //--------------------------------------------------------
                  self.myUIPicker.reloadAllComponents()
                }
                else {
                  hideOnWindow()
                  print("WrongResponce")
                }
            }
        }
    }
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        hideOnWindow()
        print("Server Fail")
    }
    //--------------------------------------------------------
    //--------------------------------------------------------
    
    //MARK:- PickerView Dalegat And Datasource Mathod
         var CheckPickerViewCount = 0
    
//    // data method to return the number of column shown in the picker.
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    // data method to return the number of row shown in the picker.
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//
//        if ChecktextField == 2 {
//
//          print(MachineSubetypeName1.count)
//        return MachineSubetypeName1.count
//        }
//
//        if ChecktextField == 1 {
//
//          print(MachineTypesNAme.count)
//        return MachineTypesNAme.count
//        }
//        return 0
//    }
//
//    // delegate method to return the value shown in the picker
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String{
//
//        if ChecktextField == 1 {
//
//          return  MachineTypesNAme[row]
//        }
//
//        if ChecktextField == 2 {
//
//          print(MachineSubetypeName1[row])
//          return MachineSubetypeName1[row]
//        }
//
//          return "Black Data"
//    }
//
//    // delegate method called when the row was selected.
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
//        //print("value: \(MachineTypesNAme[row])")
//
//        if ChecktextField == 1 {
//         txt_pasticModulMachinename.text = MachineTypesNAme[row]
//        }
//        if ChecktextField == 2 {
//            txt_TypeOfMachine.text = MachineSubetypeName1[row]
//       }
//   }
    //--------------------------------------------------------
 
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        if textField == txt_pasticModulMachinename{
//
//            ChecktextField = 1
//
//        }
//        if textField == txt_TypeOfMachine{
//
//            ChecktextField = 2
//
//        }
//
//       return true
//    }
    
    //--------------------------------------------------------
    //--------------------------------------------------------
    // MARK: - Api Call Multi ImageUpload
    func CallApiForImageUpload() {
        showOnWindow()

       print(ImageArray.count)
        var myUrl = ""

        myUrl = kBasePath + "addMachineMoulding"

        //--------------------------------------------------------
        //MARK:- ImageData
        var ImageData = [Data]()
        //--------------------------------------------------------
        for i in ImageArray{
            let fileData = UIImageJPEGRepresentation(i, 0.2)
            print(fileData!)
            ImageData.append(fileData!)
        }

        //MARK:- init paramters Dictionary------------------------
                let param = [
                              "user_id"              : _userDefault.string(forKey: "UserId")!,
                              "moulding_type"        : String(MouldingType),
                              "machine_type"         : String(MachineType),
                              "company_name"         : txt_CompanyName.text! ,
                              "year"                 : txt_ManuFYear.text!,
                              "model"                : txt_Model.text!,
                              "locking_tennage"      : txt_LockingTonnage.text!,
                              "weight"               : txt_ShotWeight.text,
                              "tie_bar_distance_1"   : TieBarDi1.text!,
                              "tie_bar_distance_2"   : txt_TieBarDis2.text!,
                              "height"               : txt_MinMouldHeight.text!,
                              "daylight"             : txt_MaxMouldHeight.text!,
                              "motor"                : txtElectricalMotor.text!,
                              "stock"                : txt_MoldStock.text!,
                              "connected_load"       : txt_ConnectedLoad.text!,
                              "latitude"             : CurrentLatitude,
                              "longitude"            : CurrentLongitude,
                              "location_address"     : txt_PlaceLocation.text!,
                              "price"                : txt_PriceNew.text!,
                              "person_name"          : txt_PersonName.text!,
                              "address"              : txt_ContectAddress.text!,
                              "phone_no"             : txt_PhoneNo.text!,
                              "email"                : txt_EmailID.text!,
                              "whatsapp_no"          : txt_WhatAppNo.text!,
                              "update_type"          : String(UpdateType)
                              ] as [String : Any]
        //--------------------------------------------------------
     print(myUrl)
     print(param)
        
    ServerCall
        .sharedInstance
        .requestMultiPartWithUrlAndParameters(myUrl,
                                              parameters: param as [String : AnyObject],
                                              fileParameterName: "image[]",
                                              fileName: "image.png",
                                              fileData: ImageArray,
                                              mimeType: "image/jpeg",
                                              delegate: self ,
                                              name: .serverCallNameUploadImage)
    }
    //--------------------------------------------------------
}
